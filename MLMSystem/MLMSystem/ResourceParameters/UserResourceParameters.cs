﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MLMSystem.ResourceParameters;

namespace MLMSystem.ResourceParameters
{
    public class UserResourceParameters : BaseResourceParameters
    {
        public override string OrderBy { get; set; } = "FirstName";

        public bool ShowClosed { get; set; }

        public long SubscriberId { get; set; }
    }
}
