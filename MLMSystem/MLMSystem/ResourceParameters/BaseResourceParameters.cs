﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.ResourceParameters
{
    public class BaseResourceParameters
    {

        const int maxPageSize = 1400;
        public int PageNumber { get; set; } = 1;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = value > maxPageSize ? maxPageSize : value;
        }

        private int _pageSize = 10;

        public string SearchQuery { get; set; }

        public virtual string OrderBy { get; set; } = "Name";

        public string Fields { get; set; }
    }
}
