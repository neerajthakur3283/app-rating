﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.BAL.DTOs
{
    public abstract class BaseResponseDto
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
