﻿using MLMSystem.DAL.Entities;
using MLMSystem.DAL.Procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.BAL.DTOs
{
    public class GetGeneralSettingsRequest : BaseEntityResourceParametersDto
    {

    }
    public class SaveGeneralSettingRequest
    {
        public int SettingId { get; set; }
        public decimal PinValue { get; set; }
        public decimal AppClickCommission { get; set; }
        public decimal RegistrationCommision { get; set; }
    }

    public class SaveGeneralSettingResponse : BaseResponseDto
    {
        public GeneralSetting GeneralSetting { get; set; }
    }

    public class GetGeneralSettingRequest : BaseEntityResourceParametersDto
    {
        public int SettingId { get; set; }
    }

    public class GetGeneralSettingResponse : BaseResponseDto
    {
        public int SettingId { get; set; }
        public decimal PinValue { get; set; }
        public decimal AppClickCommission { get; set; }
        public decimal RegistrationCommision { get; set; }
    }

}
