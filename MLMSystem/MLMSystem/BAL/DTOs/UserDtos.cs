﻿using MLMSystem.DAL.Entities;
using MLMSystem.DAL.Procedures;
using System;
using System.Collections.Generic;

namespace MLMSystem.BAL.DTOs
{
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class LoginResponse : BaseResponseDto
    {
        public User User { get; set; }
        public long UserId { get; set; }
        public string Role { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }

    public class ForgotPasswordResponse : BaseResponseDto
    {

    }

    public class SetPointsRequest
    {
        public long AppId { get; set; }
        public long UserId { get; set; }
    }

    public class SetPointsResponse : BaseResponseDto
    {

    }

    public class VerifyEmailResponse : BaseResponseDto
    {

    }
    public class DeleteUserResponse : BaseResponseDto
    {

    }
    public class ResetPasswordRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class ResetPasswordResponse : BaseResponseDto
    {

    }

    public class RegisterRequest
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public int UserType { get; set; }
    }

    public class UserProfileRequest
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public int UserType { get; set; }
        public int IdTypeUpdated { get; set; }
    }

    public class ManageGuestRequest:BaseRequest
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string AlternateMobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public int UserType { get; set; }
    }

    public class ManageGuestResponse : BaseResponseDto
    {
        public User User { get; set; }
    }

    public class ManageWithdrawlReqestRequest : BaseRequest
    {
        public long RequestId { get; set; }
        public int Status { get; set; }
        public int Point { get; set; }
        public string Comment { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string ReEnterAccountNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
        public List<long> SeletectedRequests { get; set; }
    }

    public class SaveWithdrawlCommentRequest : BaseRequest
    {
        public string Comment { get; set; }        
        public List<long> SeletectedRequests { get; set; }
    }

    public class ManagePinRequest : BaseRequest
    {
        public long RequestId { get; set; }
        public int Status { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string ReEnterAccountNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
    }

    public class ManageWithdrawlResponse : BaseResponseDto
    {
        public WithdrawlRequest WithdrawlRequest { get; set; }
    }

    public class ManagePinResponse : BaseResponseDto
    {
        public PinRequest PinRequest { get; set; }
    }

    public class ManageUserRequest
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public int UserType { get; set; }
    }

    public class ManageUserResponse : BaseResponseDto
    {
        public User User { get; set; }
    }

    public class RegisterResponse : BaseResponseDto
    {
        public User User { get; set; }
    }

    public class CheckEmailResponse : BaseResponseDto
    {
        public User User { get; set; }
        public string ReferralName { get; set; }
        public bool IsExist { get; set; }

    }

    public class CheckMobileResponse : BaseResponseDto
    {
        public User User { get; set; }
        public string ReferralName { get; set; }
        public bool IsExist { get; set; }
    }

    public class UserProfileResponse : BaseResponseDto
    {
        public User User { get; set; }
    }    

    public class GetUsersRequestDto : BaseEntityResourceParametersDto
    {
        public long? RefferalId { get; set; }
    }
    public class GetUsersResponse : BaseResponseDto
    {
        public List<ProcGetUser> Users { get; set; }
        public int Total { get; set; }
    }

    public class GetGuestsRequestDto : BaseEntityResourceParametersDto
    {
        public long? RefferalId { get; set; }
    }
    public class WithdrawlRequestsRequestDto : BaseEntityResourceParametersDto
    {
        public long? UserId { get; set; }
    }

    public class PinRequestDto : BaseEntityResourceParametersDto
    {
        public long? UserId { get; set; }
    }
    public class GetGuestsResponse : BaseResponseDto
    {
        public List<ProcGetGuest> Guests { get; set; }
        public int Total { get; set; }
    }

    public class GetWithdrawlRequest : BaseEntityResourceParametersDto
    {
        public long? UserId { get; set; }
        public int Status { get; set; }
    }

    public class GetPinRequest : BaseEntityResourceParametersDto
    {
        public long? UserId { get; set; }
        public int Status { get; set; }
    }
    public class GetWithdrawlResponse : BaseResponseDto
    {
        public List<ProcGetWithdrawlRequest> WithdrawlRequests { get; set; }
        public int Total { get; set; }
    }

    public class GetPinResponse : BaseResponseDto
    {
        public List<ProcGetPins> PinRequests { get; set; }
        public int Total { get; set; }
    }

    public class GetRewardPointsRequest : BaseEntityResourceParametersDto
    {
        public long UserId { get; set; }
    }
    public class GetRewardPointsResponse : BaseResponseDto
    {
        public List<ProcGetRewardPoint> RewardPoints { get; set; }
        public int Total { get; set; }
    }

    public class UserResourceParametersDto : BaseResourceParametersDto
    {
        public string OrderBy { get; set; }
        //public long ProviderId { get; set; }
    }

    public class GetCountriesRequestDto : BaseEntityResourceParametersDto
    {

    }
    public class GetCountriesResponse : BaseResponseDto
    {
        public List<Country> Countries { get; set; }
        
    }


}
