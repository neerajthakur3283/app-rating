﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.BAL.DTOs
{
    public abstract class BaseResourceParametersDto
    {
        const int maxPageSize = 20;
        public int PageNumber { get; set; } = 1;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = value > maxPageSize ? maxPageSize : value;
        }

        private int _pageSize = 2; // TODO : make it to 10

        public string SearchQuery { get; set; }

        public string Fields { get; set; }
    }
}
