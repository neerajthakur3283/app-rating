﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.BAL.DTOs
{
    public class EmailRequst
    {
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public class SMSRequst
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public string Code { get; set; }
    }
}
