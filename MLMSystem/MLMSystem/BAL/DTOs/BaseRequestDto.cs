﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.BAL.DTOs
{
    public class BaseRequest
    {
        public DateTimeOffset? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTimeOffset? DeletedOn { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
