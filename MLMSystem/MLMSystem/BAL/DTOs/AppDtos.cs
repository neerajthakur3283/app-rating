﻿using MLMSystem.DAL.Entities;
using MLMSystem.DAL.Procedures;
using System.Collections.Generic;

namespace MLMSystem.BAL.DTOs
{
    public class GetAppsRequest : BaseEntityResourceParametersDto
    {

    }
    public class GetAppsResponse : BaseResponseDto
    {
        public List<ProcGetApp> Apps { get; set; }
        public int Total { get; set; }
    }

    public class SaveAppRequest
    {
        public int AppId { get; set; }       
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int Point { get; set; }
    }

    public class SaveAppResponse : BaseResponseDto
    {
        public App App { get; set; }
    }

    public class GetAppRequest : BaseEntityResourceParametersDto
    {
        public long AppId { get; set; }
    }
    public class GetAppResponse : BaseResponseDto
    {
        public int AppId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int Point { get; set; }
    }

    public class DeleteAppResponse : BaseResponseDto
    {
        
    }

    public class SetAppsResponse : BaseResponseDto
    {

    }



}
