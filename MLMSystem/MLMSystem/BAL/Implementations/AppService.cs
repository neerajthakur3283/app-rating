﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.DAL;
using MLMSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MLMSystem.BAL.Implementations
{
    public class AppService : IAppService
    {
        private readonly MLMSystemDbContext _context;
        private readonly IMapper _mapper;

        public AppService(MLMSystemDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public GetAppsResponse GetApps(GetAppsRequest request)
        {
            var response = new GetAppsResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.Apps = _context.ProcGetApps
                    .FromSqlRaw($"ProcGetApps @Page, @RecsPerPage, @OrderBy,@Search, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public SaveAppResponse Save(SaveAppRequest request)
        {
            var response = new SaveAppResponse();

            try
            {
                var app = request.AppId == 0 ? new App() :
                                _context.Apps.FirstOrDefault(x => x.AppId == request.AppId);

                if (app == null)
                {
                    response.Message = "App does not exist in the db.";
                    return response;
                }


                app.Name = request.Name;
                app.Link = request.Link;
                app.Description = request.Description;
                app.Point = request.Point;
                app.IsDeleted = false;

                if (app.AppId == 0)
                {
                    _context.Apps.Add(app);
                }

                _context.SaveChanges();

                response.App = app;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }

        public DeleteAppResponse Delete(long id)
        {

            var response = new DeleteAppResponse();

            try
            {

                var app = _context.Apps.FirstOrDefault(x => x.AppId == id);

                if (app == null)
                {
                    response.Message = "App does not exist in the db.";
                    return response;
                }

                _context.Apps.Remove(app);
                _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "App deleted successfully";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
            }


            return response;
        }

        public GetAppResponse GetApp(long id)
        {
            var response = new GetAppResponse();

            try
            {
                var app = _context.Apps.FirstOrDefault(x => x.AppId == id);

                if (app == null)
                {
                    response.Message = "App does not exist in the db.";
                    return response;
                }

                response = _mapper.Map<GetAppResponse>(app);
            }
            catch (Exception)
            {

                throw;
            }

            return response;
        }

        public IEnumerable<App> GetApps()
        {
            var apps = _context.Apps.ToList();

            return apps;
        }

        public SetAppsResponse SetApps(List<int> selectedApps)
        {
            var response = new SetAppsResponse();

            try
            {

                var apps = _context.Apps.ToList();

                foreach (var app in apps)
                {
                    var existingApp = _context.Apps.FirstOrDefault(x => x.AppId == app.AppId);
                    app.Active = false;
                    _context.SaveChanges();
                }

                foreach (var appId in selectedApps)
                {
                    var app = _context.Apps.FirstOrDefault(x => x.AppId == appId);
                    app.Active = true;
                    _context.SaveChanges();
                }

                response.Success = true;
                response.Message = "Apps activated successfully";


            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
            }

            return response;
        }
        public SetAppsResponse ActivateApp(long appId, bool selected)
        {
            var response = new SetAppsResponse();

            try
            {
                var app = _context.Apps.FirstOrDefault(x => x.AppId == appId);
                app.Active = selected;
                _context.SaveChanges();

                response.Success = true;
                response.Message = selected? "App activated successfully": "App deactivated successfully";
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();

                throw;
            }

            return response;

        }

    }
}
