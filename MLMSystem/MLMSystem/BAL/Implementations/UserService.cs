﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.DAL;
using MLMSystem.DAL.Entities;
using MLMSystem.Settings;
using System;
using System.Linq;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace MLMSystem.BAL.Implementations
{
    public class UserService : IUserService
    {
        private readonly MLMSystemDbContext _context;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly IGeneralSettingService _generalSettingService;
        private readonly IOptions<ConfigSettings> _configSettings;
        private IWebHostEnvironment _hostingEnvironment;
        public UserService(MLMSystemDbContext context, IMapper mapper, IEmailService emailService, IOptions<ConfigSettings> configSettings, IGeneralSettingService generalSettingService, IWebHostEnvironment environment)
        {
            _context = context;
            _mapper = mapper;
            _emailService = emailService;
            _configSettings = configSettings;
            _generalSettingService = generalSettingService;
            _hostingEnvironment = environment;
        }

        public LoginResponse Login(LoginRequest request)
        {
            var response = new LoginResponse();

            if (request == null)
                throw new ArgumentNullException("Login request object is null");

            var user = _context.Users
                .FirstOrDefault(x => x.Email == request.Email && x.Password == request.Password);

            if (user == null)
            {
                response.Message = "User name/password is incorrect";
                return response;
            }

            if (!user.IsEmailVerified)
            {
                response.Message = "Your email is not verified";
                return response;
            }

            //var hashedPassword = BCrypt.Net.BCrypt.HashPassword(request.Password);
            // check a password

            //bool validPassword = BCrypt.Net.BCrypt.Verify(request.Password, user.Password);

            //if (!validPassword)
            //{
            //    response.Error = "Password is not correct";
            //    return response;
            //}            

            // got so far, it's a valid user

            user.LastLogin = DateTime.Now;
            _context.SaveChanges();

            response.User = user;
            response.Success = true;
            return response;
        }
        public ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest request)
        {
            var response = new ForgotPasswordResponse();

            var user = _context.Users.FirstOrDefault(x => x.Email == request.Email);

            if (user == null)
            {
                response.Message = "User does not exist";
                response.Success = false;
                return response;
            }

            //var token = GenerateToken(request.Email);
            var url = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/resetpassword";//$"test.radonreporting.com/#/unauth/reset-password/{token}";

            response.Success = _emailService.SendEmail(new EmailRequst
            {
                To = user.Email,
                Subject = "Reset Password",
                Body = $"Hi please reset your password from given link <br/>{url}"
            });

            return response;
        }
        public ResetPasswordResponse ResetPassword(ResetPasswordRequest request)
        {
            var response = new ResetPasswordResponse();

            var user = _context.Users.FirstOrDefault(x => x.Email == request.Email);

            if (user == null)
            {
                response.Message = "User does not exist";
                response.Success = false;

                return response;
            }

            //var hashedPassword = BCrypt.Net.BCrypt.HashPassword(request.Password);
            user.Password = request.Password;//hashedPassword;
            _context.SaveChanges();

            response.Success = true;

            return response;
        }
        public RegisterResponse Register(RegisterRequest request)
        {
            var response = new RegisterResponse();

            var generator = new Random();
            var token = generator.Next(0, 999999).ToString("D6");

            try
            {
                var user = request.UserId == 0 ? new User() :
                                _context.Users.FirstOrDefault(x => x.UserId == request.UserId);
                var sb = new StringBuilder();
                var verifyEmailurl = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/verify-email/{token}";//$"test.radonreporting.com/#/unauth/reset-password/{token}";

                if (user == null)
                {
                    response.Message = "User does not exist in the db.";
                    return response;
                }

                if (user.UserId == 0)
                {
                    var existingUser = _context.Users.FirstOrDefault(x => x.Email.ToLower() == request.Email.ToLower());
                    if (existingUser != null)
                    {

                        sb.Append($"Hi {user.FirstName} you are successfully registered here <br/><br/>" +
                            $"Please verify your email to login. <br/><br/>" +
                            $"{verifyEmailurl}");
                        existingUser.CountryId = request.CountryId;
                        existingUser.UserType = 2;
                        existingUser.Password = request.Password;
                        existingUser.FirstName = request.FirstName;
                        existingUser.LastName = request.LastName;
                        existingUser.Token = token;

                        response.Message = "User registered successfully";
                        _context.SaveChanges();

                        var referral = _context.Users.FirstOrDefault(x => x.UserId == request.ReferralId);
                        if (referral != null)
                        {

                            var referralRewardPoint = new RewardPoint();
                            referralRewardPoint.UserId = referral.UserId;
                            referralRewardPoint.RewardUserId = existingUser.UserId;
                            //referralRewardPoint.AppId = 0;
                            referralRewardPoint.RewardType = 1;
                            referralRewardPoint.TransactionType = 1;
                            referralRewardPoint.Point = 10;
                            _context.RewardPoints.Add(referralRewardPoint);
                            referral.RewardPoints = referral.RewardPoints + 10;
                            _context.SaveChanges();
                        }
                        response.Success = _emailService.SendEmail(new EmailRequst
                        {
                            To = existingUser.Email,
                            Subject = "Registration",
                            Body = sb.ToString()
                        });
                        response.Success = true;
                        response.User = existingUser;
                        return response;
                    }
                }


                //user.Password = request.UserId == 0 ? BCrypt.Net.BCrypt.HashPassword(request.Password) : user.Password;
                // hash and save a password
                //var hashedPassword = BCrypt.Net.BCrypt.HashPassword(request.Password);
                // check a password
                //bool validPassword = BCrypt.Net.BCrypt.Verify(request.Password, hashedPassword);
                //user.Password = hashedPassword;// request.Password;

                user.IsDeleted = false;

                if (user.UserId == 0) // create user
                {

                    user.FirstName = request.FirstName;
                    user.LastName = request.LastName;
                    user.ReferralId = request.ReferralId;
                    user.Email = request.Email;
                    user.Password = request.Password;
                    user.DOB = request.DOB;
                    user.Mobile = request.Mobile;
                    user.CountryId = request.CountryId;
                    user.UserType = 2;
                    user.IsDeleted = false;
                    _context.Users.Add(user);
                }
                else
                {
                    user.FirstName = request.FirstName;
                    user.LastName = request.LastName;
                    user.Email = request.Email;
                    user.DOB = request.DOB;
                    user.Mobile = request.Mobile;
                    user.CountryId = request.CountryId;
                    user.StateId = request.StateId;
                    user.CityId = request.CityId;
                    user.AreaCode = request.AreaCode;
                    user.Address = request.Address;
                    user.IdNo = request.IdNo;
                    user.IdType = request.IdType;
                }

                user.Token = token;

                // create/update user
                _context.SaveChanges();

                //var token = GenerateToken(request.Email);
                verifyEmailurl = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/verify-email/{token}";//$"test.radonreporting.com/#/unauth/reset-password/{token}";
                sb = new StringBuilder();
                sb.Append($"Hi {user.FirstName} you are successfully registered here <br/><br/>" +
                    $"Please verify your email to login. <br/><br/>" +
                    $"{verifyEmailurl}");

                if (request.UserId == 0)
                {
                    var referral = _context.Users.FirstOrDefault(x => x.UserId == request.ReferralId);
                    if (referral != null)
                    {
                        var referralRewardPoint = new RewardPoint();
                        referralRewardPoint.UserId = referral.UserId;
                        referralRewardPoint.RewardUserId = user.UserId;
                        //referralRewardPoint.AppId = 0;
                        referralRewardPoint.RewardType = 1;
                        referralRewardPoint.TransactionType = 1;
                        referralRewardPoint.Point = 10;
                        _context.RewardPoints.Add(referralRewardPoint);
                        referral.RewardPoints = referral.RewardPoints + 10;
                        _context.SaveChanges();
                    }
                    response.Success = _emailService.SendEmail(new EmailRequst
                    {
                        To = user.Email,
                        Subject = "Registration",
                        Body = sb.ToString()
                    });
                }


                response.User = user;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public UserProfileResponse UpdateProfile(UserProfileRequest request)
        {
            var response = new UserProfileResponse();

            try
            {
                var user = request.UserId == 0 ? new User() :
                                _context.Users.FirstOrDefault(x => x.UserId == request.UserId);

                if (user == null)
                {
                    response.Message = "User does not exist in the db.";
                    return response;
                }

                if (request.IdNo != user.IdNo || request.IdType != user.IdType)
                {
                    if (request.IdTypeUpdated == 2)
                    {
                        response.Message = "You can not update IdNo and Id Type more than 2 time.";
                        return response;
                    }
                    else
                    {
                        user.IdTypeUpdated += 1;
                    }
                }

                user.IsDeleted = false;
                user.FirstName = request.FirstName;
                user.LastName = request.LastName;
                user.Email = request.Email;
                user.DOB = request.DOB;
                user.Mobile = request.Mobile;
                user.CountryId = request.CountryId;
                user.StateId = request.StateId;
                user.CityId = request.CityId;
                user.AreaCode = request.AreaCode;
                user.Address = request.Address;
                user.IdNo = request.IdNo;
                user.IdType = request.IdType;

                // create/update user
                _context.SaveChanges();

                response.User = user;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public ManageUserResponse ManageUser(ManageUserRequest request)
        {
            var generator = new Random();
            var token = generator.Next(0, 999999).ToString("D6");

            var response = new ManageUserResponse();

            try
            {
                var user = request.UserId == 0 ? new User() :
                                _context.Users.FirstOrDefault(x => x.UserId == request.UserId);

                if (user == null)
                {
                    response.Message = "User does not exist in the db.";
                    return response;
                }

                user.IsDeleted = false;

                user.FirstName = request.FirstName;
                user.LastName = request.LastName;
                user.Email = request.Email;
                user.DOB = request.DOB;
                user.Mobile = request.Mobile;
                user.CountryId = request.CountryId;
                user.StateId = request.StateId;
                user.CityId = request.CityId;
                user.AreaCode = request.AreaCode;
                user.Address = request.Address;
                user.IdNo = request.IdNo;
                user.IdType = request.IdType;
                user.ReferralId = request.ReferralId;

                if (request.UserId == 0)
                {
                    user.Password = request.Password;
                    _context.Users.Add(user);
                    user.Token = token;
                }
                // create/update user                

                var verifyEmailurl = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/verify-email/{token}";//$"test.radonreporting.com/#/unauth/reset-password/{token}";
                var sb = new StringBuilder();
                sb.Append($"Hi {user.FirstName} you are successfully registered here <br/><br/>" +
                    $"Please verify your email to login. <br/><br/>" +
                    $"{verifyEmailurl}");

                if (request.UserId == 0)
                {
                    response.Success = _emailService.SendEmail(new EmailRequst
                    {
                        To = user.Email,
                        Subject = "Registration",
                        Body = sb.ToString()
                    });
                }

                _context.SaveChanges();

                response.User = user;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public ManageWithdrawlResponse ManageWithdrawlRequest(ManageWithdrawlReqestRequest request)
        {

            var response = new ManageWithdrawlResponse();

            try
            {
                var withdrawlRequest = request.RequestId == 0 ? new WithdrawlRequest() :
                                _context.WithdrawlRequests.FirstOrDefault(x => x.RequestId == request.RequestId);

                if (withdrawlRequest == null)
                {
                    response.Message = "withdrawlRequest does not exist in the db.";
                    return response;
                }

                withdrawlRequest.IsDeleted = false;

                withdrawlRequest.Point = request.Point;

                if (request.RequestId == 0)
                {
                    withdrawlRequest.Status = 1;
                    withdrawlRequest.CreatedBy = request.CreatedBy;
                    withdrawlRequest.Name = request.Name;
                    withdrawlRequest.AccountNumber = request.AccountNumber;
                    withdrawlRequest.IFSCCode = request.IFSCCode;
                    withdrawlRequest.Branch = request.Branch;
                    withdrawlRequest.BankName = request.BankName;
                    withdrawlRequest.Branch = request.Branch;
                    withdrawlRequest.BankAddress = request.BankAddress;

                    _context.WithdrawlRequests.Add(withdrawlRequest);
                }
                else
                {
                    withdrawlRequest.Status = request.Status;
                    withdrawlRequest.UpdatedBy = request.UpdatedBy;
                    withdrawlRequest.Comment = request.Comment;
                    var user = _context.Users.FirstOrDefault(x => x.UserId == withdrawlRequest.CreatedBy);
                    if (user != null)
                    {
                        if (request.Status == 1)
                        {
                            user.RewardPoints = user.RewardPoints - request.Point;
                        }

                    }
                }

                _context.SaveChanges();

                response.WithdrawlRequest = withdrawlRequest;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }

        public ManageWithdrawlResponse SaveWithdrawlComment(SaveWithdrawlCommentRequest request)
        {

            var response = new ManageWithdrawlResponse();

            try
            {


                var withdrawlRequests = _context.WithdrawlRequests.Where(x => string.IsNullOrEmpty(x.Comment) && request.SeletectedRequests.Contains(x.RequestId)).ToList();

                foreach (var withdrawlRequest in withdrawlRequests)
                {
                    withdrawlRequest.Comment = request.Comment;
                    _context.SaveChanges();

                }

                response.Message = "Remark saved successfully";
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public ManageGuestResponse ManageGuest(ManageGuestRequest request)
        {
            var generator = new Random();
            var token = generator.Next(0, 999999).ToString("D6");

            var response = new ManageGuestResponse();

            try
            {
                var user = request.UserId == 0 ? new User() :
                                _context.Users.FirstOrDefault(x => x.UserId == request.UserId);

                if (user == null)
                {
                    response.Message = "User does not exist in the db.";
                    return response;
                }

                user.IsDeleted = false;

                user.FirstName = request.FirstName;
                user.LastName = request.LastName;
                user.Email = request.Email;
                user.DOB = request.DOB;
                user.Mobile = request.Mobile;
                user.AlternateMobile = request.AlternateMobile;
                user.CountryId = request.CountryId;
                user.StateId = request.StateId;
                user.CityId = request.CityId;
                user.AreaCode = request.AreaCode;
                user.Address = request.Address;
                user.IdNo = request.IdNo;
                user.IdType = request.IdType;
                user.ReferralId = request.ReferralId;
                user.UserType = 3;
                if (request.UserId == 0)
                {
                    user.Password = request.Password;
                    _context.Users.Add(user);
                    user.Token = token;
                }
                // create/update user                

                var verifyEmailurl = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/verify-email/{token}";//$"test.radonreporting.com/#/unauth/reset-password/{token}";
                var sb = new StringBuilder();
                sb.Append($"Hi {user.FirstName} you are successfully registered here <br/><br/>" +
                    $"Please verify your email to login. <br/><br/>" +
                    $"{verifyEmailurl}");

                if (request.UserId == 0)
                {
                    response.Success = _emailService.SendEmail(new EmailRequst
                    {
                        To = user.Email,
                        Subject = "Registration",
                        Body = sb.ToString()
                    });
                }

                _context.SaveChanges();

                response.User = user;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public IEnumerable<Country> GetCountries()
        {
            var countries = _context.Countries.ToList();

            return countries;
        }
        public IEnumerable<State> GetStates(long countryId)
        {
            var states = _context.States.Where(x => x.CountryId == countryId).ToList();
            return states;
        }
        public IEnumerable<City> GetCites(long stateId)
        {
            var cites = _context.Cities.Where(x => x.StateId == stateId).ToList();
            return cites;
        }
        public IEnumerable<User> GetUsers()
        {
            var users = _context.Users.ToList();

            return users;
        }
        public User GetUser(long id)
        {
            var user = _context.Users.Include(x => x.Referral).FirstOrDefault(x => x.UserId == id);
            return user;
        }
        public WithdrawlRequest GetWithdrawlRequest(long id)
        {
            var withdrawlRequest = _context.WithdrawlRequests.FirstOrDefault(x => x.RequestId == id);
            return withdrawlRequest;
        }
        public PinRequest GePinRequest(long id)
        {
            var pinRequest = _context.PinRequests.FirstOrDefault(x => x.RequestId == id);
            return pinRequest;
        }
        public VerifyEmailResponse VerifyEmail(string id)
        {
            var response = new VerifyEmailResponse();

            try
            {
                var user = _context.Users.FirstOrDefault(x => x.Token == id);

                if (user == null)
                {
                    response.Message = "No account found";
                    response.Success = false;
                    return response;
                }

                if (user.IsEmailVerified)
                {
                    response.Message = "Email already verified";
                    response.Success = true;
                    return response;
                }

                user.IsEmailVerified = true;
                _context.SaveChanges();

                response.Message = "Email verified successfully";
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;

            }

            return response;
        }
        public CheckEmailResponse CheckIfEmailAlreadyExists(string email, string id)
        {
            var response = new CheckEmailResponse();
            response.IsExist = false;

            try
            {

                if (!string.IsNullOrEmpty(id) && id != "0")
                {
                    var user = _context.Users.FirstOrDefault(x => x.UserId == Convert.ToInt32(id));
                    if (user.Email.ToLower() != email.ToLower())
                    {
                        var existingUser = _context.Users.FirstOrDefault(x => x.Email.ToLower() == email.ToLower());

                        if (existingUser != null)
                        {
                            response.User = existingUser;
                            response.IsExist = true;
                            return response;
                        }
                    }
                }
                else
                {
                    var user = _context.Users.FirstOrDefault(x => x.Email.ToLower() == email.ToLower());

                    if (user != null)
                    {
                        var referral = _context.Users.FirstOrDefault(x => x.UserId == user.ReferralId);
                        if (referral != null)
                        {
                            response.ReferralName = $"{referral.FirstName} {referral.LastName}";
                        }
                        response.User = user;
                        response.IsExist = true;
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }


            return response;
        }
        public CheckMobileResponse CheckIfMobileAlreadyExists(string mobile, string id)
        {
            var response = new CheckMobileResponse();
            response.IsExist = false;

            try
            {
                if (!string.IsNullOrEmpty(id) && id != "0")
                {
                    var user = _context.Users.FirstOrDefault(x => x.UserId == Convert.ToInt32(id));
                    if (user.Mobile.ToLower() != mobile.ToLower())
                    {
                        var isExistingUser = _context.Users.FirstOrDefault(x => x.Mobile.ToLower() == mobile.ToLower());

                        if (isExistingUser != null)
                        {
                            response.User = isExistingUser;
                            response.IsExist = true;
                            return response;
                        }
                    }
                }
                else
                {
                    var user = _context.Users.FirstOrDefault(x => x.Mobile.ToLower() == mobile.ToLower());

                    if (user != null)
                    {
                        var referral = _context.Users.FirstOrDefault(x => x.UserId == user.ReferralId);
                        if (referral != null)
                        {
                            response.ReferralName = $"{referral.FirstName} {referral.LastName}";
                        }
                        response.User = user;
                        response.IsExist = true;
                        return response;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return response;
        }
        public bool CheckIfRefferalAlreadyExists(string refferalId)
        {

            if (!string.IsNullOrEmpty(refferalId))
            {
                var users = _context.Users.Where(x => x.UserId == Convert.ToInt32(refferalId));

                if (users.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }

            return false;
        }
        public GetUsersResponse GetUpgradedUsers(GetUsersRequestDto request)
        {
            var response = new GetUsersResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pReferralId = new SqlParameter("@ReferralId", request.RefferalId);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.Users = _context.ProcGetUsers
                    .FromSqlRaw($"ProcGetUpgradedUsers @Page, @RecsPerPage, @OrderBy,@Search,@ReferralId, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pReferralId, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public GetUsersResponse GetUsers(GetUsersRequestDto request)
        {
            var response = new GetUsersResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pReferralId = new SqlParameter("@ReferralId", request.RefferalId);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.Users = _context.ProcGetUsers
                    .FromSqlRaw($"ProcGetUsers @Page, @RecsPerPage, @OrderBy,@Search,@ReferralId, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pReferralId, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public PagedList<GetUsersResponse> GetUserList(UserResourceParametersDto resourceParametersDto)
        {
            var response = new PagedList<GetUsersResponse>(new List<GetUsersResponse>(), 0, 0, 0);
            try
            {
                var search = new SqlParameter("@search", resourceParametersDto.SearchQuery != null ? resourceParametersDto.SearchQuery : string.Empty);
                //var providerId = new SqlParameter("@ProviderId", resourceParametersDto.ProviderId);
                var page = new SqlParameter("@Page", resourceParametersDto.PageNumber);
                var pageCount = new SqlParameter("@RecsPerPage", resourceParametersDto.PageSize);
                var orderBy = new SqlParameter("@OrderBy", resourceParametersDto.OrderBy != null ? resourceParametersDto.OrderBy : string.Empty);
                var total = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                var userSearch = _context.ProcGetUsers
                    .FromSqlRaw($"Proc_GetUsers @Page, @RecsPerPage, @OrderBy, @search, @Total output",
                    new object[] { page, pageCount, orderBy, search, total }).ToList();

                var totalRecords = (int)total.Value;
                var searchResponse = _mapper.Map<List<GetUsersResponse>>(userSearch);
                response = new PagedList<GetUsersResponse>(searchResponse, totalRecords, resourceParametersDto.PageNumber, resourceParametersDto.PageSize);
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public GetRewardPointsResponse GetRewardPoints(GetRewardPointsRequest request)
        {
            var response = new GetRewardPointsResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pUserId = new SqlParameter("@UserId", request.UserId);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.RewardPoints = _context.ProcGetRewardPoints
                    .FromSqlRaw($"ProcGetRewardPoints @Page, @RecsPerPage, @OrderBy,@Search, @UserId, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pUserId, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public SetPointsResponse SetRewardPoints(SetPointsRequest request)
        {
            var response = new SetPointsResponse();

            try
            {
                var user = _context.Users.FirstOrDefault(x => x.UserId == request.UserId);
                var app = _context.Apps.FirstOrDefault(x => x.AppId == request.AppId);

                var rewardPoints = _context.RewardPoints.Where(x => x.AppId == request.AppId && x.UserId == request.UserId);

                if (!rewardPoints.Any())
                {
                    user.RewardPoints = user.RewardPoints + app.Point;

                    var rewardPoint = new RewardPoint();
                    rewardPoint.UserId = request.UserId;
                    rewardPoint.AppId = request.AppId;
                    rewardPoint.Point = app.Point;
                    rewardPoint.TransactionType = 1;
                    rewardPoint.RewardType = 2;
                    _context.RewardPoints.Add(rewardPoint);
                    _context.SaveChanges();

                    var referral = _context.Users.FirstOrDefault(x => x.UserId == user.ReferralId);
                    if (referral != null)
                    {
                        var referralRewardPoint = new RewardPoint();
                        referralRewardPoint.RewardUserId = request.UserId;
                        referralRewardPoint.UserId = referral.UserId;
                        referralRewardPoint.AppId = request.AppId;
                        referralRewardPoint.Point = 10;
                        referralRewardPoint.TransactionType = 1;
                        referralRewardPoint.RewardType = 2;
                        _context.RewardPoints.Add(referralRewardPoint);
                        referral.RewardPoints = referral.RewardPoints + 10;
                        _context.SaveChanges();

                    }
                }
                else
                {
                    response.Success = false;
                    response.Message = "Review already done for this app";
                    return response;
                }

                _context.SaveChanges();



                response.Success = true;
                response.Message = "Reward point added successfully";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
            }

            return response;
        }
        public DeleteUserResponse DeleteUser(long id)
        {
            var response = new DeleteUserResponse();

            try
            {

                var user = _context.Users.FirstOrDefault(x => x.UserId == id);

                if (user == null)
                {
                    response.Message = "User does not exist in the db.";
                    return response;
                }

                _context.Users.Remove(user);
                _context.SaveChangesAsync();

                response.Success = true;
                response.Message = "User deleted successfully";

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.ToString();
            }

            return response;
        }
        public GetWithdrawlResponse GetWithdrawlRequests(GetWithdrawlRequest request)
        {
            var response = new GetWithdrawlResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pUserId = new SqlParameter("@UserId", request.UserId);
                var pStatus = new SqlParameter("@Status", request.Status);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.WithdrawlRequests = _context.ProcGetWithdrawlRequests
                    .FromSqlRaw($"ProcGetWithdrawlRequests @Page, @RecsPerPage, @OrderBy,@Search,@UserId,@Status, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pUserId, pStatus, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public GetGuestsResponse GetGuests(GetGuestsRequestDto request)
        {
            var response = new GetGuestsResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pReferralId = new SqlParameter("@ReferralId", request.RefferalId);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.Guests = _context.ProcGetGuests
                    .FromSqlRaw($"ProcGetGuests @Page, @RecsPerPage, @OrderBy,@Search,@ReferralId, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pReferralId, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public bool SendEmailVerificationCode(string email, string msgBody)
        {
            return _emailService.SendEmail(new EmailRequst
            {
                To = email,
                Subject = "Email verification",
                Body = msgBody
            });
        }
        public bool SendMobileVerificationCode(string to, string code)
        {
            return _emailService.SendMobileVerificationCode(new SMSRequst
            {
                To = to,
                Subject = "Phone verification Code",
                Code = code
            });
        }
        public TakeActionResponse TakeAction(List<long> selectedApps, int action)
        {
            var response = new TakeActionResponse();
            try
            {

                var withdrawlRequests = _context.WithdrawlRequests.Where(x => x.Status == 1 && selectedApps.Contains(x.RequestId)).ToList();

                foreach (var withdrawlRequest in withdrawlRequests)
                {
                    withdrawlRequest.Status = action;
                    //_context.SaveChanges();

                    if (action == 2)
                    {
                        var rewardPoint = new RewardPoint();
                        rewardPoint.UserId = withdrawlRequest.CreatedBy.Value;
                        rewardPoint.Point = withdrawlRequest.Point;
                        rewardPoint.TransactionType = 2;
                        _context.RewardPoints.Add(rewardPoint);
                       
                        var user = _context.Users.FirstOrDefault(x => x.UserId == withdrawlRequest.CreatedBy);
                        if (user != null)
                        {
                            user.RewardPoints = user.RewardPoints - withdrawlRequest.Point;

                        }

                        _context.SaveChanges();
                    }

                }

                response.Success = true;

                if (withdrawlRequests.Any())
                {
                    if (action == 2)
                    {
                        response.Message = "Request approved successfully";
                    }
                    else if (action == 3)
                    {
                        response.Message = "Request rejected successfully";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }
        public ManagePinResponse ManagePinRequest(ManagePinRequest request)
        {
            var response = new ManagePinResponse();

            try
            {
                var pinRequest = request.RequestId == 0 ? new PinRequest() :
                                _context.PinRequests.FirstOrDefault(x => x.RequestId == request.RequestId);

                if (pinRequest == null)
                {
                    response.Message = "pinRequest does not exist in the db.";
                    return response;
                }

                pinRequest.IsDeleted = false;
                pinRequest.Amount = request.Amount;

                if (request.RequestId == 0)
                {
                    pinRequest.Status = 1;
                    pinRequest.CreatedBy = request.CreatedBy;
                    pinRequest.Name = request.Name;
                    pinRequest.AccountNumber = request.AccountNumber;
                    pinRequest.BankName = request.BankName;
                    pinRequest.BankAddress = request.BankAddress;
                    pinRequest.AccountNumber = request.AccountNumber;
                    pinRequest.IFSCCode = request.IFSCCode;
                    pinRequest.Branch = request.Branch;

                    _context.PinRequests.Add(pinRequest);
                }
                else
                {
                    pinRequest.Status = request.Status;
                    pinRequest.UpdatedBy = request.UpdatedBy;
                    if (request.Status == 2)
                    {
                        var setting = _generalSettingService.GetGeneralSetting();
                        var user = _context.Users.FirstOrDefault(x => x.UserId == pinRequest.CreatedBy);
                        var pin = Math.Round(request.Amount / setting.PinValue);
                        List<PinEntity> pinList = GenerateUniquePins(Convert.ToInt32(pin), Convert.ToInt64(user.UserId));
                        SendPinGenerationEmail(pinList, user.Email);
                    }
                }

                _context.SaveChanges();

                response.PinRequest = pinRequest;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public void SendPinGenerationEmail(List<PinEntity> pinList, string email)
        {
            string liString = string.Empty;
            foreach (var item in pinList)
            {
                liString += "<li>" + item.PIN + "</li>";
            }
            StreamReader reader = new StreamReader(_hostingEnvironment.ContentRootPath + "\\EmailTemplates\\PinListing.html");
            string msgBody = reader.ReadToEnd();
            msgBody = msgBody.Replace("{{PinList}}", liString);
            _emailService.SendEmail(new EmailRequst
            {
                To = email,
                Subject = "Pin Listing",
                Body = msgBody
            });
        }
        private List<PinEntity> GenerateUniquePins(int pinCount, long userId)
        {
            List<PinEntity> pinList = new List<PinEntity>();
            for (int i = 1; i <= pinCount; i++)
            {
                Random generator = new Random();
                string code = generator.Next(0, 999999).ToString("D6");
                string generatedPin = string.Format("PIN{0}", code);
                PinEntity pinEntity = new PinEntity
                {
                    PIN = generatedPin
                };
                pinList.Add(pinEntity);
                UserPin pin = new UserPin
                {
                    PIN = generatedPin,
                    UserId = userId
                };
                _context.UserPins.Add(pin);
            }
            _context.SaveChanges();
            return pinList;
        }
        public List<UserPin> GetUserPins(long userId)
        {
            return _context.UserPins.Where(x => x.UserId == userId && x.Status == 0).ToList();
        }
        public void UpgradeUserProfile(long userId, long pinId)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserId == userId);
            user.IsUpgraded = true;
            user.UpgradedOn = DateTime.Now;
            _context.SaveChanges();
        }
        public class PinEntity
        {
            public string PIN { get; set; }
        }
        public GetPinResponse GetPinRequests(GetPinRequest request)
        {
            var response = new GetPinResponse();

            try
            {
                var pPage = new SqlParameter("@Page", request.PageNumber);
                var pUserId = new SqlParameter("@UserId", request.UserId);
                var pStatus = new SqlParameter("@Status", request.Status);
                var pRecsPerPage = new SqlParameter("@RecsPerPage", request.PageSize);
                var pOrderBy = new SqlParameter("@OrderBy", !string.IsNullOrEmpty(request.OrderBy) ?
                        $"{request.OrderBy.ToUpper()}" : string.Empty);
                var pSearch = new SqlParameter("@Search", !string.IsNullOrEmpty(request.SearchQuery) ?
                        $"%{request.SearchQuery}%" : string.Empty);

                var pTotal = new SqlParameter
                {
                    ParameterName = "@Total",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output,
                    Size = 50,
                    Value = 0
                };

                response.PinRequests = _context.ProcGetPins
                    .FromSqlRaw($"ProcGetPinRequests @Page, @RecsPerPage, @OrderBy,@Search,@UserId,@Status, @Total output",
                    new object[] { pPage, pRecsPerPage, pOrderBy, pSearch, pUserId, pStatus, pTotal }).ToList();

                response.Total = (int)pTotal.Value;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }
        public PinRequest GetPinRequest(long id)
        {
            var pinRequest = _context.PinRequests.FirstOrDefault(x => x.RequestId == id);
            return pinRequest;
        }

    }
}
