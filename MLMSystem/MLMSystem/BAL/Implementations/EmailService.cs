﻿using Microsoft.Extensions.Options;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MLMSystem.BAL.Implementations
{
    public class EmailService : IEmailService
    {
        IOptions<ConfigSettings> _configSettings;
        public EmailService(IOptions<ConfigSettings> configSettings)
        {
            _configSettings = configSettings;
        }
        public bool SendEmail(EmailRequst requst)
        {
            try
            {
                SmtpClient client = new SmtpClient(_configSettings.Value.EmailSettings.SMTP);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(_configSettings.Value.EmailSettings.UserName, _configSettings.Value.EmailSettings.Password);
                client.Port = Convert.ToInt32(_configSettings.Value.EmailSettings.MailPort);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = Convert.ToBoolean(_configSettings.Value.EmailSettings.MailSSL);
                //client.UseDefaultCredentials = true;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(_configSettings.Value.EmailSettings.From);
                mailMessage.To.Add(requst.To);
                mailMessage.Body = requst.Body;
                mailMessage.Subject = requst.Subject;
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool SendSMS(SMSRequst reqModel)
        {
            try
            {
                //Reference url:https://prpsms.co.in/Utilities/HttpApiHelp.aspx 
                // Please refer the above link and pass the parameters according to required.


                UriBuilder builder = new UriBuilder("http://164.52.195.161");
                builder.Query = string.Format("API/SendMsg.aspx?uname={0}&pass={1}&send={2}&dest={3}&msg={4}&priority=1&schtm={5}", Utilities.SMSUserName, Utilities.SMSPassword, Utilities.SMSSenderID, reqModel.To, string.Format("Verification code for your account is {0}", reqModel.Code), DateTime.UtcNow);

                HttpClient client = new HttpClient();
                var result = client.GetAsync(builder.Uri).Result;

                using (StreamReader sr = new StreamReader(result.Content.ReadAsStreamAsync().Result))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool SendMobileVerificationCode(SMSRequst reqModel)
        {
            try
            {
                UriBuilder builder = new UriBuilder("http://164.52.195.161");
                builder.Query = string.Format("API/SendMsg.aspx?uname={0}&pass={1}&send={2}&dest={3}&msg={4}&priority=1&schtm={5}", Utilities.SMSUserName, Utilities.SMSPassword, Utilities.SMSSenderID, reqModel.To, string.Format("Your mobile verification code is {0}", reqModel.Code), DateTime.UtcNow);

                HttpClient client = new HttpClient();
                var result = client.GetAsync(builder.Uri).Result;

                using (StreamReader sr = new StreamReader(result.Content.ReadAsStreamAsync().Result))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
