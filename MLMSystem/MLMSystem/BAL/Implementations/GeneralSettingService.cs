﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.DAL;
using MLMSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MLMSystem.BAL.Implementations
{
    public class GeneralSettingService : IGeneralSettingService
    {
        private readonly MLMSystemDbContext _context;
        private readonly IMapper _mapper;

        public GeneralSettingService(MLMSystemDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public SaveGeneralSettingResponse Save(SaveGeneralSettingRequest request)
        {
            var response = new SaveGeneralSettingResponse();

            try
            {
                var app = request.SettingId == 0 ? new GeneralSetting() :
                                _context.GeneralSettings.FirstOrDefault(x => x.SettingId == request.SettingId);

                if (app == null)
                {
                    response.Message = "GeneralSetting does not exist in the db.";
                    return response;
                }

                app.PinValue = request.PinValue;
                app.AppClickCommission = request.AppClickCommission;
                app.RegistrationCommision = request.RegistrationCommision;
                app.IsDeleted = false;

                if (app.SettingId == 0)
                {
                    _context.GeneralSettings.Add(app);
                }

                _context.SaveChanges();

                response.GeneralSetting = app;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
            }

            return response;
        }


        public GetGeneralSettingResponse GetGeneralSetting()
        {
            var response = new GetGeneralSettingResponse();

            try
            {
                var app = _context.GeneralSettings.FirstOrDefault(x => x.IsDeleted != true);

                if (app == null)
                {
                    response.Message = "GeneralSetting does not exist in the db.";
                    return response;
                }

                response.SettingId = app.SettingId;
                response.AppClickCommission = app.AppClickCommission;
                response.RegistrationCommision = app.RegistrationCommision;
                response.PinValue = app.PinValue;
            }
            catch (Exception ex)
            {
                throw;
            }

            return response;
        }

        public IEnumerable<GeneralSetting> GetGeneralSettings()
        {
            var apps = _context.GeneralSettings.ToList();

            return apps;
        }
    }
}
