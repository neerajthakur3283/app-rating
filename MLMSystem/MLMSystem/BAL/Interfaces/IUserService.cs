﻿using MLMSystem.BAL.DTOs;
using MLMSystem.DAL.Entities;
using System;
using System.Collections.Generic;

namespace MLMSystem.BAL.Interfaces
{
    public interface IUserService
    {
        LoginResponse Login(LoginRequest request);
        ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest request);
        ResetPasswordResponse ResetPassword(ResetPasswordRequest request);
        RegisterResponse Register(RegisterRequest request);
        UserProfileResponse UpdateProfile(UserProfileRequest request);
        GetUsersResponse GetUsers(GetUsersRequestDto request);
        GetUsersResponse GetUpgradedUsers(GetUsersRequestDto request);
        PagedList<GetUsersResponse> GetUserList(UserResourceParametersDto resourceParametersDto);
        IEnumerable<Country> GetCountries();
        IEnumerable<State> GetStates(long countryId);
        IEnumerable<City> GetCites(long stateId);
        IEnumerable<User> GetUsers();
        CheckEmailResponse CheckIfEmailAlreadyExists(string email, string id);
        CheckMobileResponse CheckIfMobileAlreadyExists(string mobile, string id);
        bool CheckIfRefferalAlreadyExists(string refferalId);
        User GetUser(long id);
        SetPointsResponse SetRewardPoints(SetPointsRequest request);
        GetRewardPointsResponse GetRewardPoints(GetRewardPointsRequest request);
        VerifyEmailResponse VerifyEmail(string id);
        ManageUserResponse ManageUser(ManageUserRequest request);
        ManageGuestResponse ManageGuest(ManageGuestRequest request);
        DeleteUserResponse DeleteUser(long id);
        GetGuestsResponse GetGuests(GetGuestsRequestDto request);
        GetWithdrawlResponse GetWithdrawlRequests(GetWithdrawlRequest request);
        GetPinResponse GetPinRequests(GetPinRequest request);
        ManageWithdrawlResponse ManageWithdrawlRequest(ManageWithdrawlReqestRequest request);
        ManagePinResponse ManagePinRequest(ManagePinRequest request);
        ManageWithdrawlResponse SaveWithdrawlComment(SaveWithdrawlCommentRequest request);
        WithdrawlRequest GetWithdrawlRequest(long id);
        PinRequest GetPinRequest(long id);
        bool SendEmailVerificationCode(string email, string msgBody);
        bool SendMobileVerificationCode(string to, string code);
        TakeActionResponse TakeAction(List<long> selectedApps,int action);
        List<UserPin> GetUserPins(long userId);
        void UpgradeUserProfile(long userId, long pinId);
    }
}
