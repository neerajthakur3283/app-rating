﻿using MLMSystem.BAL.DTOs;
using MLMSystem.DAL.Entities;
using System.Collections.Generic;

namespace MLMSystem.BAL.Interfaces
{
    public interface IAppService
    {
        GetAppsResponse GetApps(GetAppsRequest request);
        SaveAppResponse Save(SaveAppRequest request);
        DeleteAppResponse Delete(long id);
        GetAppResponse GetApp(long id);
        IEnumerable<App> GetApps();
        SetAppsResponse SetApps(List<int> selectedApps);
        SetAppsResponse ActivateApp(long appId,bool selected);
    }
}
