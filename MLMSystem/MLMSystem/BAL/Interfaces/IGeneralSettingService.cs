﻿using MLMSystem.BAL.DTOs;
using MLMSystem.DAL.Entities;
using System.Collections.Generic;

namespace MLMSystem.BAL.Interfaces
{
    public interface IGeneralSettingService
    {
        SaveGeneralSettingResponse Save(SaveGeneralSettingRequest request);
        GetGeneralSettingResponse GetGeneralSetting();
        IEnumerable<GeneralSetting> GetGeneralSettings();
    }
}
