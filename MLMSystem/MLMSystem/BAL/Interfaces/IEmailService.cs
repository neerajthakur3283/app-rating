﻿using MLMSystem.BAL.DTOs;

namespace MLMSystem.BAL.Interfaces
{
    public interface IEmailService
    {
        public bool SendEmail(EmailRequst requst);
        public bool SendSMS(SMSRequst requst);
        public bool SendMobileVerificationCode(SMSRequst requst);

    }
}
