﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MLMSystem.ResourceParameters;
using MLMSystem.Models;
using AutoMapper;

namespace MLMSystem.Helpers
{
    public static class DataTableAjaxExtensions
    {
        private static BaseResourceParameters ToResourceParameters(this DataTableAjaxModel model)
        {
            var resourceParameters = new BaseResourceParameters
            {
                SearchQuery = model.Search.Value
            };
            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {
                    // it's a hyper link column at index 1 and in this case,
                    // jquery datatable returns index instead of name

                    //if (model is SubscriberLocationDataTableAjaxModel)
                    //{
                    //    if (column.Data == "3") column.Data = "LocationName";
                    //    if (column.Data == "4") column.Data = "ExtraLocation";
                    //    if (column.Data == "13") column.Data = "Version";
                    //}
                    //else if (model is VarDataTableAjaxModel)
                    //{
                    //    if (column.Data == "1") column.Data = "Name";
                    //}
                    //else if (model is UserDataTableAjaxModel)
                    //{
                    //    if (column.Data == "1") column.Data = "SubscriberName";
                    //}
                    //else if (model is MyPlaylistDataTableAjaxModel)
                    //{
                    //    if (column.Data == "1") column.Data = "PlaylistName";
                    //    if (column.Data == "2") column.Data = "PlaylistType";
                    //    if (column.Data == "4") column.Data = "Tracks";
                    //    if (column.Data == "5") column.Data = "DateCreated";
                    //    if (column.Data == "6") column.Data = "DateUpdated";
                    //}
                    //else if (model is PlaylistDataTableAjaxModel)
                    //{
                    //    if (column.Data == "2") column.Data = "PlaylistName";
                    //}
                    //else if (model is MessagesDataTableAjaxModel)
                    //{
                    //    if (column.Data == "3") column.Data = "DateAdded";
                    //    if (column.Data == "0") column.Data = "messageName";
                    //}
                    //else if (model is MessageListsDataTableAjaxModel)
                    //{
                    //    if (column.Data == "2") column.Data = "LastUpdated";
                    //    if (column.Data == "0") column.Data = "listName";
                    //}
                    //else if (model is MessageSchedulesDataTableAjaxModel)
                    //{
                    //    if (column.Data == "0") column.Data = "name";
                    //}
                    //else if (model is MessageCampaignsDataTableAjaxModel)
                    //{
                    //    if (column.Data == "0") column.Data = "campaignName";
                    //}
                    //else if (model is MusicSchedulesDataTableAjaxModel)
                    //{
                    //    if (column.Data == "0") column.Data = "Name";
                    //}
                    //else if (model is MusicProgramsDataTableAjaxModel)
                    //{
                    //    if (column.Data == "0") column.Data = "Name";
                    //}
                    //else if (model is SystemMessageDataTableAjaxModel)
                    //{
                    //    if (column.Data == "0") column.Data = "MessageType";
                    //    if (column.Data == "1") column.Data = "MessageText";
                    //}

                    resourceParameters.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            // calculate page number
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;

            resourceParameters.PageNumber = pageNumber;
            resourceParameters.PageSize = model.Length > 0 ? model.Length : 9999;

            return resourceParameters;
        }
        public static UserResourceParameters ToUserResourceParameters(this UserDataTableAjaxModel model)
        {
            var resourceParameters = model.ToResourceParameters();
            //var userResourceParameters = Mapper.Map<UserResourceParameters>(resourceParameters);
            ////userResourceParameters.ShowActiveUsers = model.ShowActiveUsers;
            ////userResourceParameters.VarId = model.VarId;
            return null;
        }
    }
}
