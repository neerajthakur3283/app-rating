﻿using AutoMapper;
using MLMSystem.BAL.DTOs;
using MLMSystem.DAL.Entities;
using MLMSystem.DAL.Procedures;
using MLMSystem.Models;
using System.Linq;

namespace MLMSystem.Helpers
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<LoginViewModel, LoginRequest>();
            CreateMap<LoginResponse, LoginResponseModel>();
            CreateMap<RegisterModel, RegisterRequest>();

            CreateMap<UserDataTableAjaxModel, UserResourceParametersDto>();
            CreateMap<UserDataTableAjaxModel, GetUsersRequestDto>();
            

            CreateMap<UserResourceParametersModel, UserResourceParametersDto>();
            CreateMap<ProcGetUser, GetUsersResponse>();
            CreateMap<GetUserModel, GetUsersResponse>().ReverseMap();
            CreateMap<PagedList<GetUsersResponse>, UserPagedResource>()
              .ForMember(dest => dest.Data, opt => opt.MapFrom(src => src.ToList()))
              .ForMember(dest => dest.Page, opt => opt.MapFrom(src => new Page
              {
                  PageNumber = src.CurrentPage,
                  Size = src.PageSize,
                  TotalElements = src.TotalCount,
                  TotalPages = src.TotalPages
              }));

            CreateMap<SaveAppModel, SaveAppRequest>();
            CreateMap<App, GetAppResponse>();
            CreateMap<UserProfileModel, UserProfileRequest>();
            CreateMap<ProcGetRewardPoint, GetRewardPointsResponse>();

            CreateMap<ManageUserModel, ManageUserRequest>();
            CreateMap<ManageGuestModel, ManageGuestRequest>();
            CreateMap<WidthdrawlRequestDataTableAjaxModel, WithdrawlRequest>();
            CreateMap<ManageWithdrawlRequestModel, ManageWithdrawlReqestRequest>();
            CreateMap<SaveWithdrawlCommentModel, SaveWithdrawlCommentRequest>();

            CreateMap<PinRequestDataTableAjaxModel, PinRequest>();
            CreateMap<ManagePinRequestModel, ManagePinRequest>();

        }
    }
}
