﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        $(".modalAjaxLoader").fadeIn();
    });
    $(document).ajaxComplete(function () {
        $(".modalAjaxLoader").fadeOut();
    });
});
