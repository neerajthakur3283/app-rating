﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class addreferralpoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AlternateMobile",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReferralPoints",
                table: "Users",
                nullable: false,
                defaultValue: 0);
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropColumn(
                name: "AlternateMobile",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ReferralPoints",
                table: "Users");
        }
    }
}
