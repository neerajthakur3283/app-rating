﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class updatewithdrawlrequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Branch",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IFSCCode",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReEnterAccountNumber",
                table: "WithdrawlRequests",
                nullable: true);
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {           

            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "Branch",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "IFSCCode",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "ReEnterAccountNumber",
                table: "WithdrawlRequests");
        }
    }
}
