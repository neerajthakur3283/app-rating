﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class UpdateUserTableAndPinRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsProfileUpgraded",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "BankAddress",
                table: "PinRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankName",
                table: "PinRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankAddress",
                table: "PinRequests");

            migrationBuilder.DropColumn(
                name: "BankName",
                table: "PinRequests");

            migrationBuilder.AddColumn<bool>(
                name: "IsProfileUpgraded",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
