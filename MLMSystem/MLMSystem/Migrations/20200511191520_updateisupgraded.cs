﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class updateisupgraded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUserUpgraded",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsUpgraded",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUpgraded",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsUserUpgraded",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
