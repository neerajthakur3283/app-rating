﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class addbanknameandaddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BankAddress",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankName",
                table: "WithdrawlRequests",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsUserUpgraded",
                table: "Users",
                nullable: false,
                defaultValue: false);
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankAddress",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "BankName",
                table: "WithdrawlRequests");

            migrationBuilder.DropColumn(
                name: "IsUserUpgraded",
                table: "Users");           

            
        }
    }
}
