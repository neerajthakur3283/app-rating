﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class AddColumnIsProfileUpgradedToUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsProfileUpgraded",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpgradedOn",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsProfileUpgraded",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UpgradedOn",
                table: "Users");
        }
    }
}
