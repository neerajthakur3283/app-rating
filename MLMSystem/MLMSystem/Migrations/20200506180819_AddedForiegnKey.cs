﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MLMSystem.Migrations
{
    public partial class AddedForiegnKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_ReferralId",
                table: "Users",
                column: "ReferralId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_ReferralId",
                table: "Users",
                column: "ReferralId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_ReferralId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_ReferralId",
                table: "Users");
        }
    }
}
