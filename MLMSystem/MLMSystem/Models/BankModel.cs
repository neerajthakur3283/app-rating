﻿namespace MLMSystem.Models
{
    public class BankModel
    {
        public string BRANCH { get; set; }
        public string CENTRE { get; set; }
        public string DISTRICT { get; set; }
        public string STATE { get; set; }
        public string ADDRESS { get; set; }
        public string CONTACT { get; set; }
        public bool UPI { get; set; }
        public bool RTGS { get; set; }
        public string CITY { get; set; }
        public bool NEFT { get; set; }
        public bool IMPS { get; set; }
        public string MICR { get; set; }
        public string BANK { get; set; }
        public string BANKCODE { get; set; }
        public string IFSC { get; set; }
    }
}
