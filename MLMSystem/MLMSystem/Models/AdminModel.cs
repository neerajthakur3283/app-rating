﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MLMSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.Models
{
    public class SaveAppModel
    {
        public int AppId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int Point { get; set; }
    }

    public class ManageUserModel
    {
        public long UserId { get; set; }
        public long SelectedUserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? State { get; set; }
        public long? City { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Users { get; set; }

    }

    public class ManageGuestModel
    {
        public long UserId { get; set; }
        public long SelectedUserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string AlternateMobile { get; set; }
        public long? CountryId { get; set; }
        public long? State { get; set; }
        public long? City { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Users { get; set; }

    }

    public class ManageWithdrawlRequestModel
    {
        public long RequestId { get; set; }
        public int Status { get; set; }
        public int Point { get; set; }
        public string Comment { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string ReEnterAccountNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
        public List<SelectListItem> Statuses { get; set; }
        public List<long> SeletectedRequests { get; set; }
    }

    public class SaveWithdrawlCommentModel
    {
        public string Comment { get; set; }        
        public List<long> SeletectedRequests { get; set; }
    }
    public class ManagePinRequestModel
    {
        public long RequestId { get; set; }
        public int Status { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string ReEnterAccountNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
        public List<SelectListItem> Statuses { get; set; }
    }
    public class SaveGeneralSettingModel
    {
        public int SettingId { get; set; }
        public decimal PinValue { get; set; }
        public decimal AppClickCommission { get; set; }
        public decimal RegistrationCommision { get; set; }
    }

    public class UpgradeModel
    {
        public long UserId { get; set; }
        public long PinId { get; set; }
        public List<SelectListItem> PinList { get; set; }
    }

    public class AppUserModel
    {

        public long AppUserId { get; set; }
        public long AppId { get; set; }
        public long UserId { get; set; }
        public List<SelectListItem> Users { get; set; }
        public List<SelectListItem> ActiveApps { get; set; }
        public List<SelectListItem> Apps { get; set; }

    }

    public class ActivateAppModel
    {

        public long AppId { get; set; }
        public bool Selected { get; set; }

    }
}

