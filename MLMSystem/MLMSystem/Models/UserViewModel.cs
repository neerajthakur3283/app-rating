﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.Models
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class LoginResponseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public long UserId { get; set; }
        public string Role { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AccessToken { get; set; }
        public long? ProviderId { get; set; }
        public int UserType { get; set; }
    }

    public class RecoverPasswordModel
    {
        public string Email { get; set; }
    }
    public class ResetPasswordModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class RegisterViewModel
    {
        public RegisterViewModel()
        {
            Countries = new List<SelectListItem>();
        }
        List<SelectListItem> Countries;
    }

    public class RegisterModel
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }        
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }        
        public string Email { get; set; }        
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? State { get; set; }
        public long? City { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }        
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<SelectListItem> Cities { get; set; }

    }

    public class UserProfileModel
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string ReferralName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public bool IsUpgraded { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> IdTypes { get; set; }
        public int IdTypeUpdated { get; set; }
    }

    public class UserResourceParametersModel : BaseResourceParametersModel
    {
        public string OrderBy { get; set; }
        //public long ProviderId { get; set; }
    }

    public class UserPagedResource
    {
        public List<GetUserModel> Data { get; set; }
        public Page Page { get; set; }
    }

    public class GetUserModel
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? State { get; set; }
        public long? City { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
    }
}
