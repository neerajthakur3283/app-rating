﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.Models
{
    public abstract class DataTableAjaxModel
    {
        public DataTableAjaxModel()
        {
            Columns = new List<Column>();
            Search = new Search();
            Order = new List<Order>();
        }

        // properties are not capital due to json mapping
        public int Draw { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public List<Column> Columns { get; set; }
        public Search Search { get; set; }
        public List<Order> Order { get; set; }
    }

    public class Column
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }
        public Search Search { get; set; }
    }

    public class Search
    {
        public string Value { get; set; }
        public string Regex { get; set; }
    }

    public class Order
    {
        // order by column
        public int Column { get; set; }
        // order by direction
        public string Dir { get; set; }
    }

    public class UserDataTableAjaxModel : DataTableAjaxModel
    {
        public long? ReferralId { get; set; }
        public bool? IsActive { get; set; }
    }

    public class WidthdrawlRequestDataTableAjaxModel : DataTableAjaxModel
    {
        public long? UserId { get; set; }
        public bool? IsActive { get; set; }
        public int Status { get; set; }
    }

    public class PinRequestDataTableAjaxModel : DataTableAjaxModel
    {
        public long? UserId { get; set; }
        public bool? IsActive { get; set; }
        public int Status { get; set; }
    }

    public class RewardPointDataTableAjaxModel : DataTableAjaxModel
    {
        public long? ReferralId { get; set; }
        public bool? IsActive { get; set; }
    }

    public class AppDataTableAjaxModel : DataTableAjaxModel
    {
        
    }

}
