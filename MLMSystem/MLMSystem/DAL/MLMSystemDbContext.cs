﻿using Microsoft.EntityFrameworkCore;
using MLMSystem.DAL.Entities;
using MLMSystem.DAL.Procedures;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL
{
    public class MLMSystemDbContext : DbContext
    {
        public MLMSystemDbContext(DbContextOptions<MLMSystemDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<App> Apps { get; set; }
        public DbSet<RewardPoint> RewardPoints { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<WithdrawlRequest> WithdrawlRequests { get; set; }
        public DbSet<PinRequest> PinRequests { get; set; }
        public DbSet<GeneralSetting> GeneralSettings { get; set; }
        public DbSet<UserPin> UserPins { get; set; }
        //Procedures

        public DbSet<ProcGetUser> ProcGetUsers { get; set; }
        public DbSet<ProcGetApp> ProcGetApps { get; set; }
        public DbSet<ProcGetRewardPoint> ProcGetRewardPoints { get; set; }
        public DbSet<ProcGetGuest> ProcGetGuests { get; set; }
        public DbSet<ProcGetWithdrawlRequest> ProcGetWithdrawlRequests { get; set; }
        public DbSet<ProcGetPins> ProcGetPins { get; set; }

        public override int SaveChanges()
        {
            var timestamp = DateTime.Now;

            var entities = ChangeTracker.Entries().Where(x => x.Entity is EntityBase && (x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted));
            try
            {

                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {
                        ((EntityBase)entity.Entity).CreatedOn = timestamp;
                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        ((EntityBase)entity.Entity).UpdatedOn = timestamp;
                    }
                    else if (entity.State == EntityState.Deleted)
                    {
                        ((EntityBase)entity.Entity).DeletedOn = timestamp;
                        entity.State = EntityState.Modified;
                    }
                }

                return base.SaveChanges();
            }
            catch (ValidationException validationException)
            {
                throw validationException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
