﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class GeneralSetting : EntityBase
    {
        [Key]
        public int SettingId { get; set; }
        [MaxLength(100)]
        public decimal PinValue { get; set; }
        public decimal AppClickCommission { get; set; }
        public decimal RegistrationCommision { get; set; }
    }
}
