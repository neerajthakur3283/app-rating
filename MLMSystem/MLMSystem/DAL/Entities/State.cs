﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class State
    {
        [Key]
        public int StateId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public int CountryId { get; set; }
        
    }
}
