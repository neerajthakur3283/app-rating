﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class Guest : EntityBase
    {
        [Key]
        public long GuestId { get; set; }
        public long RefferalId { get; set; }
        [MaxLength(15)]
        public string MobileNo { get; set; }
        [MaxLength(15)]
        public string AlternateMobileNo { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }       

    }
}
