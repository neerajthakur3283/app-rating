﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class RewardPoint : EntityBase
    {
        [Key]
        public long RewardPointId { get; set; }
        public long UserId { get; set; }
        public long AppId { get; set; }
        public int Point { get; set; }
        public int TransactionType { get; set; }
        public int RewardType { get; set; }
        public long? RewardUserId { get; set; }
    }
}
