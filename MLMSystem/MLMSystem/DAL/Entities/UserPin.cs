﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class UserPin : EntityBase
    {
        [Key]
        public long PinId { get; set; }
        public string PIN { get; set; }
        public int Status { get; set; }
        public long UserId { get; set; }
    }
}
