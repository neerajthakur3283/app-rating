﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    
    public class City
    {
        [Key]
        public int CityId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public int StateId { get; set; }
    }
}
