﻿using System.ComponentModel.DataAnnotations;

namespace MLMSystem.DAL.Entities
{
    public class App : EntityBase
    {
        [Key]
        public int AppId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string Link { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public int Point { get; set; }
        public bool Active { get; set; }

    }
}
