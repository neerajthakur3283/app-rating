﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class User:EntityBase
    {
        [Key]
        public long UserId { get; set; }
        [MaxLength(100)]
        public string Password { get; set; }
        public long ReferralId { get; set; }
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(15)]
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        [MaxLength(10)]
        public string IdNo { get; set; }
        public string IdType { get; set; }
        public int UserType { get; set; }
        public int RewardPoints { get; set; }
        public DateTime? LastLogin { get; set; }
        public string Token { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsMobileVerified { get; set; }
        public int IdTypeUpdated { get; set; }
        public int ReferralPoints { get; set; }
        public string AlternateMobile { get; set; }
        public bool IsUpgraded { get; set; }
        public DateTime? UpgradedOn { get; set; }

        [ForeignKey("ReferralId")]
        public virtual User Referral { get; set; }
    }
}
