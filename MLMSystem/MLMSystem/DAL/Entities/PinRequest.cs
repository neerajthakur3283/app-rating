﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Entities
{
    public class PinRequest : EntityBase
    {
        [Key]
        public long RequestId { get; set; }        
        public int Status { get; set; }
        public decimal Amount { get; set; }
        [MaxLength(500)]
        public string Comment { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string AccountNumber { get; set; }
        public string ReEnterAccountNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }

    }
}
