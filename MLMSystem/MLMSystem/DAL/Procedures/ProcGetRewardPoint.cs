﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Procedures
{
    public class ProcGetRewardPoint
    {
        [Key]
        public long Id { get; set; }
        public int AppId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Link { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Point { get; set; }

        public string ReviewDate
        {
            get
            {
                return CreatedOn.ToShortDateString();
            }
        }
    }
}
