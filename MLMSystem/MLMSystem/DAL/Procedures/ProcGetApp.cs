﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Procedures
{
    public class ProcGetApp
    {
        [Key]
        public long Id { get; set; }
        public int AppId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int Point { get; set; }
        public bool Active { get; set; }
    }
}
