﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.DAL.Procedures
{
    public class ProcGetUser
    {
        [Key]
        public long Id { get; set; }
        public long UserId { get; set; }
        public string Password { get; set; }        
        public long ReferralId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public int? AreaCode { get; set; }
        public string Address { get; set; }
        public string IdNo { get; set; }
        public string IdType { get; set; }
    }
}
