﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MLMSystem.DAL.Procedures
{
    public class ProcGetPins
    {
        [Key]
        public long Id { get; set; }
        public long RequestId { get; set; }
        public int Status { get; set; }
        public decimal Amount { get; set; }        
        public string Comment { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }        
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public string RequestedDate
        {
            get
            {
                return CreatedOn!=null? CreatedOn.Value.ToString():"";
            }
        }
        public string ActionDate
        {
            get
            {
                return UpdatedOn != null ? UpdatedOn.Value.ToString() : "";
            }
        }

        public string RequestStatus
        {
            get
            {
                return Status==1?"Pending":(Status == 2?"Approved":"Rejected");
            }
        }
    }
}
