﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLMSystem.Settings
{
    public class ConfigSettings
    {
        public EmailSettings EmailSettings { get; set; }
        public JwtSettings JwtSettings { get; set; }
        public SiteSettings SiteSettings { get; set; }
    }
    public class EmailSettings
    {
        public string SMTP { get; set; }
        public string MailPort { get; set; }
        public string MailSSL { get; set; }
        public string From { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class JwtSettings
    {
        public string Secret { get; set; }
    }
    public class SiteSettings
    {
        public string BaseUrl { get; set; }
    }

    public static class Utilities
    {
        public const string SMSUserName = "20200598";
        public const string SMSPassword = "cGmI2Vw9";
        public const string SMSSenderID = "20200598";


    }
}
