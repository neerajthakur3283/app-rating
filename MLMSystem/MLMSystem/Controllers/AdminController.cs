﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.DAL;
using MLMSystem.Helpers;
using MLMSystem.Models;
using MLMSystem.Settings;

namespace MLMSystem.Controllers
{

    [Authorize]
    [Route("admin")]
    public class AdminController : BaseController
    {

        private readonly ILogger<UsersController> _logger;
        private readonly IOptions<ConfigSettings> _configSettings;
        private readonly IUserService _userService;
        private readonly IAppService _appService;
        private readonly IGeneralSettingService _generalSettingService;
        private readonly IMapper _mapper;
        private readonly MLMSystemDbContext _dbContext;
        private IWebHostEnvironment _hostingEnvironment;

        public AdminController(ILogger<UsersController> logger, IOptions<ConfigSettings> configSettings,
            IUserService userService, IAppService appService, IGeneralSettingService generalSettingService,
            IMapper mapper, MLMSystemDbContext dbContext, IWebHostEnvironment environment)
        {
            _configSettings = configSettings;
            _userService = userService;
            _appService = appService;
            _generalSettingService = generalSettingService;
            _mapper = mapper;
            _logger = logger;
            _dbContext = dbContext;
            _hostingEnvironment = environment;
            _logger.LogDebug(1, "NLog injected into AdminController");
        }

        // GET: Admin
        public ActionResult Index()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }

        // GET: Admin
        [Route("apps")]
        public ActionResult GetApps()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }
        
        [Route("all-transactions")]
        public ActionResult AllTransactions()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("all-transactions")]
        public IActionResult GetAllTransactions(UserDataTableAjaxModel model)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetRewardPointsRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                UserId = 0

            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetRewardPoints(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.RewardPoints
            });

        }

        // GET: Admin
        [Route("loadapps")]
        public ActionResult LoadApps()
        {
            return View();
        }

        [HttpPost]        
        [Route("loadapps")]
        public IActionResult LoadApps(AppDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetAppsRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _appService.GetApps(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Apps
            });

        }

        [HttpGet]
        [Route("create-app/{id?}")]
        public ActionResult CreateApp(long? id)
        {
            var model = new SaveAppModel
            {

            };

            if (id != null)
            {
                var app = _appService.GetApp(id.Value);

                if (app != null)
                {
                    model.AppId = app.AppId;
                    model.Name = app.Name;
                    model.Description = app.Description;
                    model.Point = app.Point;
                    model.Link = app.Link;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("create-app")]
        public IActionResult CreateApp(SaveAppModel model)
        {
            if (ModelState.IsValid)
            {

                // prepare request object
                var registerRequest = _mapper.Map<SaveAppRequest>(model);

                // TODO: use automapper here
                var registerResponse = _appService.Save(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    app = registerResponse.App
                });
            }

            // bind all errors (if any) message(s) from modelstate 
            var modelStateErrors = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter email and password."
            });
        }

        // GET: Admin
        [HttpPost]
        [Route("delete-app/{id}")]
        public ActionResult DeleteApp(long id)
        {
            var response = _appService.Delete(id);

            return Json(new
            {
                success = response.Success,
                message = response.Message
            });
        }

        [HttpPost]
        [Route("delete-guest/{id}")]
        public ActionResult DeleteGuest(long id)
        {
            var response = _userService.DeleteUser(id);

            return Json(new
            {
                success = response.Success,
                message = response.Message
            });
        }

        [HttpPost]
        [Route("delete-user/{id}")]
        public ActionResult DeleteUser(long id)
        {
            var response = _userService.DeleteUser(id);

            return Json(new
            {
                success = response.Success,
                message = response.Message
            });
        }
        // GET: Admin
        [Route("upgradedusers")]
        public ActionResult UpgradedUsers()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("upgradedusers")]
        public IActionResult UpgradedUsers(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetUsersRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetUpgradedUsers(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Users
            });

        }
        // GET: Admin
        [Route("myusers")]
        public ActionResult MyUsers()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("myusers")]
        public IActionResult MyUsers(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetUsersRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = CurrentUser.UserId
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetUsers(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Users
            });

        }

        [Route("dashboard")]
        public ActionResult Dashboard()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            //var user = _userService.GetUser(CurrentUser.UserId);
            //ViewBag.MyPoints = user.RewardPoints;
            return View();
        }
        // GET: Admin
        [Route("users")]
        public ActionResult GetUsers()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("getusers")]
        public IActionResult GetUsers(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetUsersRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetUsers(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Users
            });

        }

        // GET: Admin
        [Route("guests")]
        public ActionResult Guests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("getguests")]
        public IActionResult GetGuests(UserDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetGuestsRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                //RefferalId = CurrentUser.UserId                
                RefferalId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetGuests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Guests
            });

        }        

        [HttpGet]
        [Route("manage-guest/{id?}")]
        public IActionResult ManageGuest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            //var id = CurrentUser?.UserId;

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            var users = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select User" } };
            users.AddRange(_userService.GetUsers().Where(x => x.UserType == 1 || x.UserType == 2).Select(x =>
                new SelectListItem
                {
                    Value = x.UserId.ToString(),
                    Text = $"{x.FirstName} {x.LastName}-{x.Email}"
                }).ToList());

            var model = new ManageGuestModel
            {
                Countries = countries,
                Users = users
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.SelectedUserId = user.ReferralId;
                    model.Mobile = user.Mobile;
                    model.AlternateMobile = user.AlternateMobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit Guest";
            }
            else
            {
                ViewBag.ModalTitle = "Create Guest";
            }


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-guest")]
        public IActionResult ManageGuest(ManageGuestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var registerRequest = _mapper.Map<ManageGuestRequest>(model);
                registerRequest.ReferralId = model.SelectedUserId;
                registerRequest.CreatedBy = Convert.ToInt32(CurrentUser.UserId);

                // TODO: use automapper here
                var response = _userService.ManageGuest(registerRequest);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    user = response.User
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }


        [HttpGet]
        [Route("manage-user/{id?}")]
        public IActionResult ManageUser(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            //var id = CurrentUser?.UserId;

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            var users = new List<SelectListItem>();
            users.AddRange(_userService.GetUsers().Where(x => x.UserType == 1 || x.UserType == 2).Select(x =>
                    new SelectListItem
                    {
                        Value = x.UserId.ToString(),
                        Text = $"{x.FirstName} {x.LastName}-{x.Email}"
                    }).ToList());

            var model = new ManageUserModel
            {
                Countries = countries,
                Users = users
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.SelectedUserId = user.ReferralId;
                    model.Mobile = user.Mobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit User";
            }
            else
            {
                ViewBag.ModalTitle = "Create User";
            }


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-user")]
        public IActionResult ManageUser(ManageUserModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var registerRequest = _mapper.Map<ManageUserRequest>(model);

                // TODO: use automapper here
                var registerResponse = _userService.ManageUser(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    user = registerResponse.User
                });
            }

            // bind all errors (if any) message(s) from modelstate 
            var modelStateErrors = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter email and password."
            });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("profile")]
        public IActionResult GetProfile()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            var id = CurrentUser?.UserId;

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            var model = new UserProfileModel
            {
                Countries = countries
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.Mobile = user.Mobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit User";
            }
            else
            {
                ViewBag.ModalTitle = "Create User";
            }


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("profile")]
        public IActionResult UpdateProfile(UserProfileModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var registerRequest = _mapper.Map<UserProfileRequest>(model);

                // TODO: use automapper here
                var registerResponse = _userService.UpdateProfile(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    user = registerResponse.User
                });
            }

            // bind all errors (if any) message(s) from modelstate 
            var modelStateErrors = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter email and password."
            });
        }

        // GET: Admin
        [Route("assign-apps")]
        public ActionResult AssignAppsToUser()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            var users = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select User" } };
            users.AddRange(_userService.GetUsers().Select(x =>
                new SelectListItem
                {
                    Value = x.UserId.ToString(),
                    Text = $"{x.FirstName} {x.LastName}"
                }).ToList());

            //var apps = _appService.GetApps().ToList();
            var apps = _appService.GetApps().Select(x => new SelectListItem
            {
                Value = x.AppId.ToString(),
                Text = x.Name
            }).ToList();

            //var apps = _appService.GetApps().ToList();
            var activeApps = _appService.GetApps().Where(x => x.Active == true).Select(x => new SelectListItem
            {
                Value = x.AppId.ToString(),
                Text = x.Name
            }).ToList();

            ViewBag.ActiveApps = activeApps;

            var model = new AppUserModel
            {
                Users = users,
                Apps = apps,
                ActiveApps = activeApps
            };


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("SetApps")]
        public IActionResult SetApps(List<int> selectedIds)
        {

            var response = _appService.SetApps(selectedIds);

            var activeApps = _appService.GetApps().Where(x => x.Active == true).Select(x => new SelectListItem
            {
                Value = x.AppId.ToString(),
                Text = x.Name
            }).ToList();

            return Json(new
            {
                success = response.Success,
                message = response.Message,
                activeApps

            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("takeaction")]
        public IActionResult TakeAction(List<long> selectedIds, int action)
        {

            var response = _userService.TakeAction(selectedIds, action);

            //var activeApps = _appService.GetApps().Where(x => x.Active == true).Select(x => new SelectListItem
            //{
            //    Value = x.AppId.ToString(),
            //    Text = x.Name
            //}).ToList();

            return Json(new
            {
                success = response.Success,
                message = response.Message,


            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("ActivateApp")]
        public IActionResult ActivateApp(ActivateAppModel model)
        {

            var response = _appService.ActivateApp(model.AppId, model.Selected);

            var activeApps = _appService.GetApps().Where(x => x.Active == true).Select(x => new SelectListItem
            {
                Value = x.AppId.ToString(),
                Text = x.Name
            }).ToList();

            return Json(new
            {
                success = response.Success,
                message = response.Message,
                activeApps

            });
        }

        // GET: Users/Create
        [AllowAnonymous]
        [Route("Login")]
        public ActionResult Login()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Login")]
        public IActionResult Login([FromForm]LoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {

                // prepare request object
                var loginRequest = _mapper.Map<LoginRequest>(model);

                // TODO: use automapper here
                var loginResponse = _userService.Login(loginRequest);

                if (loginResponse.Success)
                {
                    var currentUser = new LoginResponseModel();
                    currentUser.UserId = loginResponse.User.UserId;
                    currentUser.Email = loginResponse.User.Email;
                    currentUser.UserType = loginResponse.User.UserType;
                    currentUser.FirstName = loginResponse.User.FirstName;
                    currentUser.LastName = loginResponse.User.LastName;

                    HttpContext.Session.SetObject("LoggedInUser", currentUser);

                    var userClaims = new List<Claim>()
                {
                    new Claim("UserName", loginResponse.User.Email),
                    new Claim(ClaimTypes.Name, loginResponse.User.FirstName),
                    new Claim(ClaimTypes.Email, loginResponse.User.Email),
                    //new Claim(ClaimTypes.DateOfBirth, user.DateOfBirth),
                    new Claim(ClaimTypes.Role, loginResponse.User.UserType==1?"Admin":"User")
                 };

                    var userIdentity = new ClaimsIdentity(userClaims, "User Identity");

                    var userPrincipal = new ClaimsPrincipal(new[] { userIdentity });
                    HttpContext.SignInAsync(userPrincipal);
                    //var user = HttpContext.Session.GetObject<User>("LoggedInUser");

                    return Json(new
                    {
                        success = loginResponse.Success,
                        userType = loginResponse.User.UserType
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = loginResponse.Success,
                        message = loginResponse.Message
                    });
                }


            }

            return Json(new
            {
                success = false
            });
        }

        [Route("WithdrawlRequests")]
        public ActionResult WithdrawlRequests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "0", Text = "All" },
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };

            var model = new ManageWithdrawlRequestModel
            {
                Statuses = statuses
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("GetWithdrawlRequests")]
        public IActionResult GetWithdrawlRequests(WidthdrawlRequestDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetWithdrawlRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                //RefferalId = CurrentUser.UserId   
                Status = model.Status,
                UserId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetWithdrawlRequests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.WithdrawlRequests
            });

        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        [Route("savecommentmodal/{id}")]
        public IActionResult savecommentmodal(string id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");

            var model = new SaveWithdrawlCommentModel
            {

            };

            HttpContext.Session.SetString("SelectedIds", id);
            return PartialView("SaveComment", model);
        }

        [HttpPost]
        [Route("SaveComment")]
        public IActionResult SaveComment(SaveWithdrawlCommentModel model)
        {
            var selectedIds = HttpContext.Session.GetString("SelectedIds");
            List<long> seletectedRequests = selectedIds.Split(',').Select(long.Parse).ToList();
            model.SeletectedRequests = seletectedRequests;

            // prepare request object
            var request = _mapper.Map<SaveWithdrawlCommentRequest>(model);
            request.UpdatedBy = Convert.ToInt32(CurrentUser.UserId);

            // TODO: use automapper here
            var response = _userService.SaveWithdrawlComment(request);
            HttpContext.Session.SetString("SelectedIds", string.Empty);

            return Json(new
            {
                success = response.Success,
                message = response.Message,
                withdrawlRequest = response.WithdrawlRequest
            });
        }

        [HttpGet]
        [Route("manage-withdrawl-request/{id?}")]
        public IActionResult ManageWithdrawlRequest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            //var id = CurrentUser?.UserId;

            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };
            //countries.AddRange(_userService.GetCountries().Select(x =>
            //    new SelectListItem
            //    {
            //        Value = x.CountryId.ToString(),
            //        Text = x.Name,
            //        Selected = (x.Name == "India")
            //    }).ToList());            

            var model = new ManageWithdrawlRequestModel
            {
                Statuses = statuses
            };

            if (id > 0)
            {
                var user = _userService.GetWithdrawlRequest(id.Value);
                if (user != null)
                {
                    model.RequestId = user.RequestId;
                    model.Point = user.Point;
                    model.Status = user.Status;
                    model.Comment = user.Comment;
                    model.Name = user.Name;
                    model.AccountNumber = user.AccountNumber;
                    model.IFSCCode = user.IFSCCode;
                    model.Branch = user.Branch;

                }
                ViewBag.modaltitle = "Edit Withdrawl Request";
            }
            else
            {
                ViewBag.modaltitle = "Create Withdrawl Request";
            }
            return View(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Route("manage-withdrawl-request")]
        //public IActionResult ManageWithdrawlRequest(ManageWithdrawlRequestModel model)

        [Route("settings")]
        public IActionResult GeneralSettings()
        {
            var response = _generalSettingService.GetGeneralSetting();
            var model = new SaveGeneralSettingModel
            {
                SettingId = response.SettingId,
                PinValue = response.PinValue,
                RegistrationCommision = response.RegistrationCommision,
                AppClickCommission = response.AppClickCommission
            };

            return View(model);
        }

        [Route("PinRequests")]
        public ActionResult PinRequests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };

            var model = new ManagePinRequestModel
            {
                Statuses = statuses
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("GetPinRequests")]
        public IActionResult GetPinRequests(PinRequestDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetPinRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                //RefferalId = CurrentUser.UserId   
                Status = model.Status,
                UserId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetPinRequests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.PinRequests
            });

        }

        [HttpGet]
        [Route("manage-pin-request/{id?}")]
        public IActionResult ManagePinRequest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Admin");
            
            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };

            var model = new ManagePinRequestModel
            {
                Statuses = statuses
            };

            if (id > 0)
            {
                var user = _userService.GetPinRequest(id.Value);
                if (user != null)
                {
                    model.RequestId = user.RequestId;
                    model.Amount = user.Amount;
                    model.Status = user.Status;
                    model.Comment = user.Comment;
                    model.Name = user.Name;
                    model.AccountNumber = user.AccountNumber;
                    model.IFSCCode = user.IFSCCode;
                    model.Branch = user.Branch;

                }
                ViewBag.modaltitle = "Edit Pin Request";
            }
            else
            {
                ViewBag.modaltitle = "Create Pin Request";
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-pin-request")]
        public IActionResult ManagePinRequest(ManagePinRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var request = _mapper.Map<ManagePinRequest>(model);
                request.UpdatedBy = Convert.ToInt32(CurrentUser.UserId);

                // TODO: use automapper here
                var response = _userService.ManagePinRequest(request);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    withdrawlRequest = response.PinRequest
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("update-generalsettings")]
        public IActionResult GeneralSettings(SaveGeneralSettingModel model)
        {
            if (ModelState.IsValid)
            {

                // prepare request object
                var registerRequest = new SaveGeneralSettingRequest
                {
                    SettingId = model.SettingId,
                    AppClickCommission = model.AppClickCommission,
                    RegistrationCommision = model.RegistrationCommision,
                    PinValue = model.PinValue
                };

                // TODO: use automapper here
                var registerResponse = _generalSettingService.Save(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    app = registerResponse.GeneralSetting
                });
            }

            // bind all errors (if any) message(s) from modelstate
            var modelStateErrors = string.Join("; ", ModelState.Values
            .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter valid input."
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-withdrawl-request")]
        public IActionResult ManageWithdrawlRequest(ManageWithdrawlRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var request = _mapper.Map<ManageWithdrawlReqestRequest>(model);
                request.UpdatedBy = Convert.ToInt32(CurrentUser.UserId);

                // TODO: use automapper here
                var response = _userService.ManageWithdrawlRequest(request);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    withdrawlRequest = response.WithdrawlRequest
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }
        
    }
}
