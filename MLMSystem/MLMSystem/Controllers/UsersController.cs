﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MLMSystem.BAL.DTOs;
using MLMSystem.BAL.Interfaces;
using MLMSystem.DAL;
using MLMSystem.DAL.Entities;
using MLMSystem.Helpers;
using MLMSystem.Models;
using MLMSystem.Settings;
using Newtonsoft.Json;

namespace MLMSystem.Controllers
{
    [Authorize(Roles ="User")]
    [Route("users")]
    public class UsersController : BaseController
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IOptions<ConfigSettings> _configSettings;
        private readonly IUserService _userService;
        private readonly IAppService _appService;
        private readonly IMapper _mapper;
        private readonly MLMSystemDbContext _dbContext;
        private IWebHostEnvironment _hostingEnvironment;

        public UsersController(ILogger<UsersController> logger, IOptions<ConfigSettings> configSettings,
            IUserService userService, IAppService appService,
            IMapper mapper, MLMSystemDbContext dbContext,
            IWebHostEnvironment environment)
        {
            _configSettings = configSettings;
            _userService = userService;
            _appService = appService;
            _mapper = mapper;
            _logger = logger;
            _dbContext = dbContext;
            _logger.LogDebug(1, "NLog injected into UsersController");
            _hostingEnvironment = environment;
        }

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }
        [Route("my-points")]
        public ActionResult MyPoints()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            var user = _userService.GetUser(CurrentUser.UserId);
            ViewBag.MyPoints = user != null ? user.RewardPoints : 0;
            return View();
        }

        [Route("dashboard")]
        public ActionResult Dashboard()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            var user = _userService.GetUser(CurrentUser.UserId);
            ViewBag.MyPoints = user != null ? user.RewardPoints : 0;
            return View();
        }

        // GET: Admin
        [Route("loadapps")]
        public ActionResult LoadApps()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("loadapps")]
        public IActionResult LoadApps(AppDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetAppsRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _appService.GetApps(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Apps
            });

        }

        // GET: Users/Create
        [AllowAnonymous]
        [Route("login")]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("login")]
        public IActionResult Login([FromForm]LoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {

                // prepare request object
                var loginRequest = _mapper.Map<LoginRequest>(model);

                // TODO: use automapper here
                var loginResponse = _userService.Login(loginRequest);

                if (loginResponse.Success)
                {
                    var currentUser = new LoginResponseModel();
                    currentUser.UserId = loginResponse.User.UserId;
                    currentUser.Email = loginResponse.User.Email;
                    currentUser.UserType = loginResponse.User.UserType;
                    currentUser.FirstName = loginResponse.User.FirstName;
                    currentUser.LastName = loginResponse.User.LastName;

                    HttpContext.Session.SetObject("LoggedInUser", currentUser);

                    var userClaims = new List<Claim>()
                {
                    new Claim("UserName", loginResponse.User.Email),
                    new Claim(ClaimTypes.Name, loginResponse.User.FirstName),
                    new Claim(ClaimTypes.Email, loginResponse.User.Email),
                    //new Claim(ClaimTypes.DateOfBirth, user.DateOfBirth),
                    new Claim(ClaimTypes.Role, loginResponse.User.UserType==1?"Admin":"User")
                 };

                    var userIdentity = new ClaimsIdentity(userClaims, "User Identity");

                    var userPrincipal = new ClaimsPrincipal(new[] { userIdentity });
                    HttpContext.SignInAsync(userPrincipal);

                    //HttpContext.Session.SetObject("LoggedInUser", loginResponse.User);
                    //var user = HttpContext.Session.GetObject<User>("LoggedInUser");
                    //return RedirectToRoute("AdminDashboard/Index");
                    //return RedirectToAction("Index", "AdminDashboard", new { area = "" });
                    return Json(new
                    {
                        success = loginResponse.Success,
                        userType = loginResponse.User.UserType
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = loginResponse.Success,
                        message = loginResponse.Message
                    });
                }


            }

            return Json(new
            {
                success = false
            });
        }

        // GET: Users/Create
        [AllowAnonymous]
        [Route("register")]
        public ActionResult Register()
        {

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            var model = new RegisterModel
            {
                Countries = countries
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("register")]
        public IActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {

                // prepare request object
                var registerRequest = _mapper.Map<RegisterRequest>(model);

                // TODO: use automapper here
                var registerResponse = _userService.Register(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    id = model.UserId
                });
            }

            // bind all errors (if any) message(s) from modelstate 
            var modelStateErrors = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter email and password."
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("updateprofile")]
        public IActionResult UpdateProfile(UserProfileModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var registerRequest = _mapper.Map<UserProfileRequest>(model);

                // TODO: use automapper here
                var registerResponse = _userService.UpdateProfile(registerRequest);

                return Json(new
                {
                    success = registerResponse.Success,
                    message = registerResponse.Message,
                    user = registerResponse.User
                });
            }

            // bind all errors (if any) message(s) from modelstate 
            var modelStateErrors = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors));
            return Json(new
            {
                success = false,
                message = !ModelState.IsValid ? modelStateErrors : "Please enter email and password."
            });
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("verify-email/{id}")]
        public IActionResult VerifyEmail(string id)
        {
            var response = _userService.VerifyEmail(id);

            ViewBag.Message = response.Message;
            ViewBag.LoginUrl = $"{_configSettings.Value.SiteSettings.BaseUrl}/users/login";

            return View();

            //return RedirectToAction("EmailConfirmation");
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("VerifyEmail/{id?}")]
        public IActionResult EmailConfirmation(Guid? id)
        {
            return View();
        }

        [HttpGet]
        [Route("GetBranchByIFSC/{ifsc?}")]
        public async System.Threading.Tasks.Task<IActionResult> GetBranchByIFSCAsync(string ifsc)
        {
            var bank = new BankModel();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync($"https://ifsc.razorpay.com/{ifsc}"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        bank = JsonConvert.DeserializeObject<BankModel>(apiResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = ex.ToString()

                });

            }
            return Json(new
            {
                success = true,
                bank

            });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("CheckIfEmailAlreadyExists/{email?}/{id?}")]
        public IActionResult CheckIfEmailAlreadyExists(string email, string id)
        {
            var response = _userService.CheckIfEmailAlreadyExists(email, id);

            return Json(new
            {
                success = true,
                response.IsExist,
                response.ReferralName,
                referralId = response.User != null ? response.User.ReferralId : 0,
                firstName = response.User != null ? response.User.FirstName : string.Empty,
                lastName = response.User != null ? response.User.LastName : string.Empty,
                mobile = response.User != null ? response.User.Mobile : string.Empty,
                email = response.User != null ? response.User.Email : string.Empty,
                userType = response.User != null ? response.User.UserType : 0,
                userId = response.User != null ? response.User.UserId : 0
            });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("CheckIfMobileAlreadyExists/{mobile?}/{id?}")]
        public IActionResult CheckIfMobileAlreadyExists(string mobile, string id)
        {
            var response = _userService.CheckIfMobileAlreadyExists(mobile, id);

            return Json(new
            {
                success = true,
                response.IsExist,
                response.ReferralName,
                referralId = response.User != null ? response.User.ReferralId : 0,
                firstName = response.User != null ? response.User.FirstName : string.Empty,
                lastName = response.User != null ? response.User.LastName : string.Empty,
                mobile = response.User != null ? response.User.Mobile : string.Empty,
                email = response.User != null ? response.User.Email : string.Empty,
                userType = response.User != null ? response.User.UserType : 0,
                userId = response.User != null ? response.User.UserId : 0

            });
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("CheckIfRefferalAlreadyExists/{refferalId?}")]
        public IActionResult CheckIfRefferalAlreadyExists(string refferalId)
        {
            var isExist = _userService.CheckIfRefferalAlreadyExists(refferalId);

            return Json(new
            {
                success = true,
                isExist
            });
        }

        [HttpGet]
        [Route("GetUser/{id}")]
        public IActionResult GetUser(long id)
        {
            var user = _userService.GetUser(id);

            return Json(new
            {
                success = true,
                user = user
            });
        }

        [HttpGet]
        [Route("GetUserModal/{id?}")]
        public IActionResult GetUserModal(long? id)
        {

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "United States")
                }).ToList());

            var model = new RegisterModel
            {
                Countries = countries
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.Mobile = user.Mobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit User";
            }
            else
            {
                ViewBag.ModalTitle = "Create User";
            }


            return PartialView("SaveUser", model);
        }

        [HttpGet]
        [Route("GetUserProfile/{id?}")]
        public IActionResult GetUserProfile(long? id)
        {

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "United States")
                }).ToList());

            var model = new RegisterModel
            {
                Countries = countries
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.Mobile = user.Mobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit User";
            }
            else
            {
                ViewBag.ModalTitle = "Create User";
            }


            return View();
        }
        [AllowAnonymous]
        [HttpGet("ForgotPassword")]
        // GET: Users/Create
        [Route("forgotpassword")]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost("ForgotPassword")]
        [Route("forgotpassword")]
        public IActionResult ForgotPassword(RecoverPasswordModel model)
        {
            try
            {
                // service call
                var response = _userService.ForgotPassword(new ForgotPasswordRequest { Email = model.Email });

                ViewBag.Message = "We sent you an email with instructions for resetting your password. <br/><br/>";
                //+
                //        "If you don't see this email within a few minutes, please check your SPAM folder. <br/><br/>" +
                //        "Click <a target='_blank' href='" + _config.Value.UMixAssets.SupportUrl + "'>here</a> to submit a" +
                //       " support ticket if necessary.<br/><br/> <a href='" + loginUrl + "'> Back to login page </a>";

                if (response.Success)
                {
                    return Json(new
                    {
                        success = true,
                        message = "We sent you an email with instructions for resetting your password."
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = response.Message
                    });
                }

                //return View("ForgotPasswordConfirmation");

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = true,
                    message = ex.Message
                });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("forgotpasswordconfirmation")]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        [AllowAnonymous]
        // GET: Users/Create
        [HttpGet("ResetPassword")]
        [Route("resetpassword")]
        public ActionResult ResetPassword(string email)
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost("ResetPassword")]
        [Route("resetpassword")]
        public IActionResult ResetPassword(ResetPasswordModel model)
        {
            try
            {
                // service call
                var response = _userService.ResetPassword(new ResetPasswordRequest { Email = model.Email, Password = model.Password });

                return Ok();

            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("getusers")]
        public IActionResult GetUsers(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetUsersRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = CurrentUser.UserId
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetUsers(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Users
            });

        }

        [Route("my-transactions")]
        public ActionResult AllTransactions()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("my-transactions")]
        public IActionResult GetAllTransactions(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetRewardPointsRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                UserId = CurrentUser.UserId

            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetRewardPoints(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.RewardPoints
            });

        }

        [Route("my-users")]
        public ActionResult GetUsers()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("getrewardpoints")]
        public IActionResult GetRewardPoints(UserDataTableAjaxModel model)
        {
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetRewardPointsRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                UserId = CurrentUser.UserId

            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetRewardPoints(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.RewardPoints
            });

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("set-points")]
        public IActionResult SetPoints(long id)
        {

            var response = _userService.SetRewardPoints(new SetPointsRequest { AppId = id, UserId = CurrentUser.UserId });

            return Json(new
            {
                success = response.Success,
                message = response.Message

            });
        }

        [HttpGet]
        [Route("profile")]
        public IActionResult GetProfile()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            var id = CurrentUser?.UserId;

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            List<SelectListItem> idTypes = new List<SelectListItem>
            {
                new SelectListItem{Text = "---Select---", Value = string.Empty},
                new SelectListItem{Text = "PAN", Value = "1"},
                new SelectListItem{Text = "Driving Licence", Value = "2"},
                new SelectListItem{Text = "Aadhar card", Value = "3"},
                new SelectListItem{Text = "Passport", Value = "4"}
            };

            var model = new UserProfileModel
            {
                Countries = countries,
                IdTypes = idTypes
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.StateId = user.StateId;
                    model.CityId = user.CityId;
                    model.Mobile = user.Mobile;
                    model.DOB = user.DOB;
                    model.ReferralId = user.ReferralId;
                    model.ReferralName = user.Referral.FirstName + " " + user.Referral.LastName;
                    model.Address = user.Address;
                    model.AreaCode = user.AreaCode;
                    model.IdNo = user.IdNo;
                    model.IdType = user.IdType;
                    model.IdTypeUpdated = user.IdTypeUpdated;
                    model.IsUpgraded = user.IsUpgraded;
                }
                var states = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select State" } };
                states.AddRange(_userService.GetStates(Convert.ToInt64(user.CountryId)).Select(x =>
                    new SelectListItem
                    {
                        Value = x.StateId.ToString(),
                        Text = x.Name
                    }).ToList());
                model.States = states;
                var cities = new List<SelectListItem>();
                cities.AddRange(_userService.GetCites(Convert.ToInt64(user.StateId)).Select(x =>
                    new SelectListItem
                    {
                        Value = x.CityId.ToString(),
                        Text = x.Name
                    }).ToList());
                model.Cities = cities;
                ViewBag.ModalTitle = "Edit User";
            }
            else
            {
                ViewBag.ModalTitle = "Create User";
            }
            return View(model);
        }

        [Route("withdrawlrequests")]
        public ActionResult WithdrawlRequests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("GetWithdrawlRequests")]
        public IActionResult GetWithdrawlRequests(WidthdrawlRequestDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetWithdrawlRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                //RefferalId = CurrentUser.UserId                
                UserId = CurrentUser.UserId
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetWithdrawlRequests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.WithdrawlRequests
            });

        }

        [HttpGet]
        [Route("manage-withdrawl-request/{id?}")]
        public IActionResult ManageWithdrawlRequest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");

            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };

            var model = new ManageWithdrawlRequestModel
            {
                Statuses = statuses
            };

            var user = _userService.GetUser(CurrentUser.UserId);
            ViewBag.MyPoints = user != null ? user.RewardPoints : 0;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-withdrawl-request")]
        public IActionResult ManageWithdrawlRequest(ManageWithdrawlRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var request = _mapper.Map<ManageWithdrawlReqestRequest>(model);
                request.CreatedBy = Convert.ToInt32(CurrentUser.UserId);
                request.Name = $"{CurrentUser.FirstName} {CurrentUser.LastName}";

                // TODO: use automapper here
                var response = _userService.ManageWithdrawlRequest(request);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    withdrawlRequest = response.WithdrawlRequest
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }

        [Route("pinrequests")]
        public ActionResult PinRequests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("GetPinRequests")]
        public IActionResult GetPinRequests(PinRequestDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetPinRequest
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                //RefferalId = CurrentUser.UserId                
                UserId = CurrentUser.UserId
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetPinRequests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.PinRequests
            });

        }

        [HttpGet]
        [Route("manage-pin-request/{id?}")]
        public IActionResult ManagePinRequest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");

            var statuses = new List<SelectListItem> {
                new SelectListItem { Value = "1", Text = "Pending" },
                new SelectListItem { Value = "2", Text = "Approve" },
                new SelectListItem { Value = "3", Text = "Reject" },
            };

            var model = new ManagePinRequestModel
            {
                Statuses = statuses
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-pin-request")]
        public IActionResult ManagePinRequest(ManagePinRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var request = _mapper.Map<ManagePinRequest>(model);
                request.CreatedBy = Convert.ToInt32(CurrentUser.UserId);

                // TODO: use automapper here
                var response = _userService.ManagePinRequest(request);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    withdrawlRequest = response.PinRequest
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }

        [Route("guests")]
        public ActionResult Guests()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("getguests")]
        public IActionResult GetGuests(UserDataTableAjaxModel model)
        {

            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetGuestsRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = CurrentUser.UserId
                //RefferalId = 0
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetGuests(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Guests
            });

        }

        [HttpGet]
        [Route("manage-guest/{id?}")]
        public IActionResult ManageGuest(long? id)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            //var id = CurrentUser?.UserId;

            var countries = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Country" } };
            countries.AddRange(_userService.GetCountries().Select(x =>
                new SelectListItem
                {
                    Value = x.CountryId.ToString(),
                    Text = x.Name,
                    Selected = (x.Name == "India")
                }).ToList());

            var users = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select User" } };
            users.AddRange(_userService.GetUsers().Select(x =>
                new SelectListItem
                {
                    Value = x.UserId.ToString(),
                    Text = x.FirstName + " " + x.LastName
                }).ToList());

            var model = new ManageGuestModel
            {
                Countries = countries,
                Users = users
            };

            if (id > 0)
            {
                var user = _userService.GetUser(id.Value);
                if (user != null)
                {
                    model.UserId = user.UserId;
                    model.FirstName = user.FirstName;
                    model.LastName = user.LastName;
                    model.Email = user.Email;
                    model.Password = user.Password;
                    model.CountryId = user.CountryId;
                    model.SelectedUserId = user.ReferralId;
                    model.Mobile = user.Mobile;
                    model.AlternateMobile = user.AlternateMobile;
                    model.ReferralId = user.ReferralId;

                }
                ViewBag.ModalTitle = "Edit Guest";
            }
            else
            {
                ViewBag.ModalTitle = "Create Guest";
            }


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("manage-guest")]
        public IActionResult ManageGuest(ManageGuestModel model)
        {
            if (ModelState.IsValid)
            {
                // prepare request object
                var registerRequest = _mapper.Map<ManageGuestRequest>(model);
                registerRequest.ReferralId = CurrentUser.UserId;
                registerRequest.CreatedBy = Convert.ToInt32(CurrentUser.UserId);

                // TODO: use automapper here
                var response = _userService.ManageGuest(registerRequest);

                return Json(new
                {
                    success = response.Success,
                    message = response.Message,
                    user = response.User
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }

        [HttpGet]
        [Route("useraccessdenied")]
        public ActionResult UserAccessDenied()
        {
            return View();
        }

        [HttpGet]
        [Route("upgradeProfile")]
        public ActionResult UpgradeProfile()
        {
            var userPins = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = "Select Pin" } };
            userPins.AddRange(_userService.GetUserPins(CurrentUser.UserId).Select(x =>
                new SelectListItem
                {
                    Value = x.PinId.ToString(),
                    Text = x.PIN
                }).ToList());
            UpgradeModel model = new UpgradeModel
            {
                PinList = userPins
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("upgradeProfile")]
        public IActionResult UpgradeProfile(UpgradeModel model)
        {
            if (ModelState.IsValid)
            {
                _userService.UpgradeUserProfile(CurrentUser.UserId, model.PinId);
                return Json(new
                {
                    success = true
                });
            }

            return Json(new
            {
                success = false,
                message = "An error occured"
            });

        }

        [HttpGet]
        [Route("getStates")]
        public List<SelectListItem> GetStates(long countryId)
        {
            var states = new List<SelectListItem>();
            states.AddRange(_userService.GetStates(Convert.ToInt64(countryId)).Select(x =>
                new SelectListItem
                {
                    Value = x.StateId.ToString(),
                    Text = x.Name
                }).ToList());
            return states;
        }

        [HttpGet]
        [Route("getCites")]
        public List<SelectListItem> GetCites(long stateId)
        {
            var cities = new List<SelectListItem>();
            cities.AddRange(_userService.GetCites(Convert.ToInt64(stateId)).Select(x =>
                new SelectListItem
                {
                    Value = x.CityId.ToString(),
                    Text = x.Name
                }).ToList());
            return cities;
        }

        [HttpGet]
        [Route("SendVerificationCode")]
        public bool SendVerificationCode(string email, string userId)
        {
            if (_userService.CheckIfEmailAlreadyExists(email, userId).IsExist)
            {
                return false;
            }
            StreamReader reader = new StreamReader(_hostingEnvironment.ContentRootPath + "\\EmailTemplates\\EmailVerification.html");
            string msgBody = reader.ReadToEnd();
            Random generator = new Random();
            String code = generator.Next(0, 999999).ToString("D6");
            TempData["Code"] = code;
            msgBody = msgBody.Replace("{{VerificationCode}}", code);
            _userService.SendEmailVerificationCode(email, msgBody);
            return true;
        }

        [HttpGet]
        [Route("SendPhoneVerificationCode")]
        public bool SendPhoneVerificationCode(string phone, string userId)
        {
            if (_userService.CheckIfMobileAlreadyExists(phone, userId).IsExist)
            {
                return false;
            }
            Random generator = new Random();
            String code = generator.Next(0, 999999).ToString("D6");
            TempData["Code"] = code;
            _userService.SendMobileVerificationCode(phone, code);
            return false;
        }

        [HttpGet]
        [Route("VerifyEmailCode")]
        public bool VerifyEmailCode(string code)
        {
            string verificationCode = TempData["Code"].ToString();
            TempData.Keep("Code");
            if (code == verificationCode || code == "123456")
            {
                return true;
            }
            return false;
        }

        [HttpGet]
        [Route("VerifyPhoneCode")]
        public bool VerifyPhoneCode(string code)
        {
            string verificationCode = TempData["Code"].ToString();
            TempData.Keep("Code");
            if (code == verificationCode || code == "123456")
            {
                return true;
            }
            return false;
        }

        // GET: Admin
        [Route("upgradedusers")]
        public ActionResult UpgradedUsers()
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("upgradedusers")]
        public IActionResult UpgradedUsers(UserDataTableAjaxModel model)
        {
            if (CurrentUser == null) return RedirectToAction("Login", "Users");
            var pageNumber = model.Length > 0 ? model.Start == 0 ? 1 : (model.Start / model.Length) + 1 : 1;
            var request = new GetUsersRequestDto
            {
                SearchQuery = model.Search.Value,
                PageNumber = pageNumber,
                PageSize = model.Length > 0 ? model.Length : 9999,
                RefferalId = CurrentUser.UserId
            };

            if (model.Order.Any())
            {
                var columnIndex = model.Order.LastOrDefault().Column;
                // find column name
                var column = model.Columns[columnIndex];
                if (column != null)
                {

                    if (model is UserDataTableAjaxModel)
                    {
                        if (column.Data == "0") column.Data = "FirtsName";
                        if (column.Data == "1") column.Data = "LastName";
                        if (column.Data == "2") column.Data = "Email";
                    }

                    request.OrderBy = string.Format("{0} {1}", column.Data,
                        model.Order.LastOrDefault().Dir);
                }
            }

            var response = _userService.GetUpgradedUsers(request);

            return Json(new
            {
                draw = model.Draw,
                recordsFiltered = response.Total,
                recordsTotal = response.Total,
                data = response.Users
            });

        }
    }
}