﻿using Microsoft.AspNetCore.Mvc;
using MLMSystem.Helpers;
using MLMSystem.Models;

namespace MLMSystem.Controllers
{
    public class BaseController : Controller
    {       
        protected LoginResponseModel CurrentUser
        {
            get            
            {
                var user = HttpContext.Session.GetObject<LoginResponseModel>("LoggedInUser");
                return user;
            }
        }
    }
}